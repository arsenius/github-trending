package com.gojek.assignment.github.trending.core.di

import android.content.Context
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepositoryImpl
import com.gojek.assignment.github.trending.core.retrofit.ApiBuilder
import com.gojek.assignment.github.trending.core.retrofit.ApiContextImpl
import org.koin.dsl.module

fun Modules(context: Context) = module {
    val apiContext = ApiContextImpl(context)
    single<GithubTrendingRepository> { GithubTrendingRepositoryImpl(apiContext, ApiBuilder(apiContext).build()) }
}