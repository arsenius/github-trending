package com.gojek.assignment.github.trending.core.viewmodel

import com.airbnb.mvrx.*
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository
import com.gojek.assignment.github.trending.data.state.GithubTrendingState
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class GitHubTrendingViewModel(
    initialState: GithubTrendingState,
    private val githubRepo: GithubTrendingRepository
) : MvRxViewModel<GithubTrendingState>(initialState) {

    enum class Sort {
        ByStars,
        ByName
    }

    fun sortBy(sort: Sort) = withState { state ->
        Single.fromCallable {
                when (sort) {
                    Sort.ByStars -> state.repos.sortedByDescending {
                        it.stars.toInt()
                    }
                    else -> state.repos.sortedBy {
                        it.name.toLowerCase()
                    }
                }
        }.subscribeOn(Schedulers.computation())
            .execute {
                if(it is Loading)
                    copy(reposRequest = GithubTrendingState.LoadingType.Reloading to Loading())
                else
                    copy(
                        repos = if(it is Success) it() ?: repos else repos,
                        expandedIndex = null,
                        reposRequest = GithubTrendingState.LoadingType.Completed to Uninitialized
                    )
            }
    }

    fun expandItem(index: Int) = setState {
        copy(
            expandedIndex =
                if (index >= repos.size || index < 0 || index == expandedIndex)
                    null
                else index
        )
    }

    fun reloadRepos() {
            githubRepo.retrieveRepositories().execute {
                if(it is Fail)
                    copy(
                        reposRequest = GithubTrendingState.LoadingType.Completed to it,
                        mainErrorMessage = getErrorMessage(it.error)
                    )
                else {
                    val loadedRepos = it()
                    val type =
                        if(it is Loading)
                            GithubTrendingState.LoadingType.Reloading
                        else
                            GithubTrendingState.LoadingType.Completed
                    copy(
                        reposRequest = type to it,
                        repos = loadedRepos ?: repos,
                        expandedIndex = null,
                        mainErrorMessage =
                            if(it is Success && loadedRepos.isNullOrEmpty())
                                githubRepo.errorMessages.unknownErrorMessage
                            else
                                ""
                    )
                }
            }
    }

    private fun getErrorMessage(error: Throwable) =
        if(!githubRepo.isNetworkAvailable)
            githubRepo.errorMessages.offlineErrorMessage
        else
            error.message ?: githubRepo.errorMessages.unknownErrorMessage

    companion object : MvRxViewModelFactory<GitHubTrendingViewModel, GithubTrendingState> {

        override fun create(viewModelContext: ViewModelContext, state: GithubTrendingState): GitHubTrendingViewModel {
            val api: GithubTrendingRepository by viewModelContext.activity.inject()
            return GitHubTrendingViewModel(state, api)
        }
    }
}