package com.gojek.assignment.github.trending.core.repositories

import com.gojek.assignment.github.trending.core.retrofit.ApiContext
import com.gojek.assignment.github.trending.data.model.GithubRepo
import io.reactivex.Single

/*
    This is just outline for scalable architecture
 */

interface GithubTrendingRepository {

    val isNetworkAvailable: Boolean
    fun retrieveRepositories(): Single<List<GithubRepo>>
    val errorMessages: ApiContext.ErrorMessages
}