package com.gojek.assignment.github.trending.ui.fragments


import androidx.appcompat.widget.Toolbar
import com.airbnb.mvrx.BaseMvRxActivity
import com.airbnb.mvrx.BaseMvRxFragment


abstract class BaseFragment: BaseMvRxFragment() {

    private val parentActivity by lazy {
        requireActivity() as BaseMvRxActivity
    }

    protected fun setupActionBar(toolbar: Toolbar) {
        parentActivity.setSupportActionBar(toolbar)
    }

}