package com.gojek.assignment.github.trending.core.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.generic.RoundingParams
import com.facebook.drawee.view.SimpleDraweeView

val RecyclerView.currentScrollPosition: Int
    get() {
        val lm = layoutManager
        return if(lm is LinearLayoutManager)
            lm.findFirstCompletelyVisibleItemPosition()
        else
            -1
    }

var RecyclerView.isScrollEnabled: Boolean
    get() = if(tag is Boolean) tag as Boolean else true
    set(enabled) {
        setOnTouchListener { _, _ -> !enabled }
        tag = enabled
    }

fun RecyclerView.onScrollChangedState(onStateChanged: (isScrollableToBottom: Boolean,
                                                       isScrollableToTop: Boolean) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            onStateChanged(
                recyclerView.canScrollVertically(1),
                recyclerView.canScrollVertically(-1)
            )
        }
    })
}


