package com.gojek.assignment.github.trending.data.model

import com.squareup.moshi.Json

data class GithubRepo (
    @Json(name = "author")
    val author: String,
    @Json(name = "name")
    val name: String,
    @Json(name = "url")
    val url: String,
    @Json(name = "description")
    val description: String,
    @Json(name = "language")
    val language: String?,
    @Json(name = "languageColor")
    val languageColor: String?,
    @Json(name = "stars")
    val stars: String,
    @Json(name = "forks")
    val forks: String,
    @Json(name = "currentPeriodStars")
    val currentPeriodStars: String,
    @Json(name = "builtBy")
    val builtBy: List<BuiltBy>
) {
    data class BuiltBy (
        @Json(name = "username")
        val username: String,
        @Json(name = "href")
        val url: String,
        @Json(name = "avatar")
        val avatarUrl: String
    )
}