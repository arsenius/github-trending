package com.gojek.assignment.github.trending.data.state

import com.airbnb.mvrx.MvRxState

data class BasicUiState (
    val scrollingPosition: Int = 0,
    val canScroll: Boolean = false,
    val scrollToPosition: Boolean = false
): MvRxState