package com.gojek.assignment.github.trending.core.databinding

import android.graphics.Color
import android.net.Uri
import android.view.View
import androidx.databinding.BindingAdapter
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.gojek.assignment.github.trending.R
import java.lang.Exception
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.image.ImageInfo
import com.gojek.assignment.github.trending.core.extensions.roundedAsCircle


@BindingAdapter("imageUrl")
fun setImageUrl(view: SimpleDraweeView, imageUrl: String) {
    try {
        val resize = view.context.resources.getInteger(R.integer.resize_avatar_option)
        view.roundedAsCircle = true
        val controller = Fresco.newDraweeControllerBuilder()
            .setImageRequest(ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(imageUrl))
                .setResizeOptions(ResizeOptions(resize, resize))
                .build())
            .setControllerListener(object: BaseControllerListener<ImageInfo>() {
                override fun onFailure(id: String?, throwable: Throwable?) {
                    super.onFailure(id, throwable)
                    view.roundedAsCircle = false
                }
            })
            .build()
        view.controller = controller

    } catch (e: Exception) {
        e.printStackTrace()
    }
}

@BindingAdapter("shapeBgColor")
fun setShapeDrawableBgColor(view: View, hexColor: String?) {
    if(hexColor == null)
        return
    try {
        val bgColor = Color.parseColor(hexColor)
        when(val background = view.background) {
            is ShapeDrawable -> background.paint.color = bgColor
            is GradientDrawable -> background.setColor(bgColor)
            is ColorDrawable -> background.color = bgColor
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}