package com.gojek.assignment.github.trending.ui.controllers

import com.airbnb.epoxy.EpoxyController
import com.airbnb.mvrx.withState
import com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel
import com.gojek.assignment.github.trending.data.model.GithubRepo
import com.gojek.assignment.github.trending.data.state.GithubTrendingState
import com.gojek.assignment.github.trending.rowEmpty
import com.gojek.assignment.github.trending.rowGithubRepo
import com.gojek.assignment.github.trending.rowGithubRepoLoading
import com.gojek.assignment.github.trending.rowMainError


class GithubTrendingReposController(private val viewModel: GitHubTrendingViewModel): EpoxyController() {

    private var onDescriptionClick: (String) -> Unit = {}
    private var onRefresh = {}

    fun setOnDescriptionClick(listener: (url: String) -> Unit) {
        onDescriptionClick = listener
    }

    fun setOnRefresh(listener: () -> Unit) {
        onRefresh = listener
    }


    override fun buildModels() = withState(viewModel) {
        if(it.mainErrorMessage.isNotEmpty())
        {
            rowMainError {
                id(0)
                error(it.mainErrorMessage)
                onRetryClick { _ ->
                    onRefresh()
                }
            }
            return@withState
        }

        when(it.reposRequest.first) {
            GithubTrendingState.LoadingType.Reloading -> renderSkeleton()
            else -> renderRepos(it.repos, it.expandedIndex)
        }

    }

    private fun renderSkeleton() {
        for(index in 0..15)
            rowGithubRepoLoading {
                id(index)
            }
    }

    private fun renderRepos(repos: List<GithubRepo>, expandedIndex: Int?) {
        val lastIndex = repos.size - 1
        repos.forEachIndexed { index, githubRepo ->
            rowGithubRepo {
                id(index)
                model(githubRepo)
                isLastRow(index == lastIndex)
                isExpanded(expandedIndex == index)
                onItemClick { _ ->
                    viewModel.expandItem(index)
                }
                onDescriptionClick { _ ->
                    withState(viewModel) {
                        onDescriptionClick(it.repos[index].url)
                    }
                }
            }
        }

        rowEmpty {
            id(lastIndex + 1)
        }
    }
}