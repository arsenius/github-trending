package com.gojek.assignment.github.trending.core.retrofit.interceptors

import android.util.Log
import com.gojek.assignment.github.trending.core.retrofit.ApiContext
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import java.io.File
import java.util.concurrent.TimeUnit


class InterceptorsProvider(private val context: ApiContext) {

    private val headerCacheControl = "Cache-Control"
    private val httpCache = "http-cache"
    private val headerPragma = "Pragma"
    private val cacheSize = 15728640L
    private val cacheExpirationInHours = 2


    val cache: Cache?
        get() {
            var cache: Cache? = null
            try {
                cache = Cache(File(context.cacheDir, httpCache), cacheSize)
            } catch (e: Exception) {
                Log.e(InterceptorsProvider::class.simpleName, "Couldn't create cache!")
            }

            return cache
        }

    val offlineCacheInterceptor: Interceptor
        get() = Interceptor { chain ->

        var request = chain.request()

        if (!context.isNetworkAvailable) {
            val cacheControl = CacheControl.Builder()
                .maxStale(cacheExpirationInHours, TimeUnit.HOURS)
                .build()
            request = request.newBuilder()
                .removeHeader(headerPragma)
                .removeHeader(headerCacheControl)
                .cacheControl(cacheControl)
                .build()
        }

        chain.proceed(request)
    }
}