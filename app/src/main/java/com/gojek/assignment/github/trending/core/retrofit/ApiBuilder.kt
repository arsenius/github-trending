package com.gojek.assignment.github.trending.core.retrofit

import com.gojek.assignment.github.trending.core.retrofit.interceptors.InterceptorsProvider
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


class ApiBuilder(private val context: ApiContext) {

    private val moshi: Moshi
        get() = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

    private val client: OkHttpClient
        get() {
            val interceptorsProvider = InterceptorsProvider(context)
            return if (context.cacheDir.isEmpty())
                OkHttpClient.Builder().build()
            else
                OkHttpClient
                    .Builder()
                    .cache(interceptorsProvider.cache)
                    .addInterceptor(interceptorsProvider.offlineCacheInterceptor)
                    .build()
        }

    private val retrofit: Retrofit
        get() = Retrofit.Builder()
            .client(client)
            .baseUrl(context.baseUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()


    @Suppress("NON_PUBLIC_CALL_FROM_PUBLIC_INLINE")
    inline fun <reified T> build() =
        retrofit.create(T::class.java)

}