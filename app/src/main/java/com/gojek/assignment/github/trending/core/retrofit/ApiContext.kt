package com.gojek.assignment.github.trending.core.retrofit

interface ApiContext {

    interface ErrorMessages {
        val offlineErrorMessage: String
        val unknownErrorMessage: String
    }

    val baseUrl: String
    val cacheDir: String
    val isNetworkAvailable: Boolean
    val errorMessages: ErrorMessages
}