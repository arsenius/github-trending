package com.gojek.assignment.github.trending.core.viewmodel

import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import com.gojek.assignment.github.trending.data.state.BasicUiState

class BasicUiViewModel (initialState: BasicUiState) : MvRxViewModel<BasicUiState>(initialState) {

    fun onScrollStateChanged(isScrollableToBottom: Boolean, isScrollableToTop: Boolean) = setState {
        copy(
            canScroll = isScrollableToBottom && isScrollableToTop,
            scrollToPosition = false
        )
    }

    fun scrollToTop() = setState {
        copy(
            scrollToPosition = canScroll
        )
    }

    fun setCurrentScrollPosition(position: Int) {
        if(position == 0 || position == -1)
            setState {
                copy(
                    canScroll = false,
                    scrollToPosition = false
                )
            }
    }


    companion object : MvRxViewModelFactory<BasicUiViewModel, BasicUiState> {
        override fun create(viewModelContext: ViewModelContext, state: BasicUiState) =
            BasicUiViewModel(state)
    }
}