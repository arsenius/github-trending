package com.gojek.assignment.github.trending.core.resources

import androidx.annotation.IdRes

interface MenuManager {
    val menuResId: Int
    fun onMenuItemSelected(@IdRes menuId: Int)
}