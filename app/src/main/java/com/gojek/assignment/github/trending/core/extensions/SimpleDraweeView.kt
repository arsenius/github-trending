package com.gojek.assignment.github.trending.core.extensions

import com.facebook.drawee.generic.RoundingParams
import com.facebook.drawee.view.SimpleDraweeView

var SimpleDraweeView.roundedAsCircle: Boolean
    get() = hierarchy.roundingParams?.roundAsCircle ?: false
    set(rounded) {
        hierarchy.roundingParams = RoundingParams().apply {
            roundAsCircle = rounded
        }
    }