package com.gojek.assignment.github.trending.core.retrofit

import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService
import com.gojek.assignment.github.trending.R

class ApiContextImpl(private val context: Context) : ApiContext {
    override val baseUrl: String
        get() = context.getString(R.string.api_base_url)
    override val cacheDir: String
        get() = context.cacheDir.absolutePath
    override val isNetworkAvailable: Boolean
        get() {
            val manager = getSystemService(context, ConnectivityManager::class.java)
            val networkInfo = manager?.activeNetworkInfo
            return (networkInfo != null && networkInfo.isConnected)
        }
    override val errorMessages: ApiContext.ErrorMessages
        get() = object: ApiContext.ErrorMessages {
            override val offlineErrorMessage: String
                get() = context.getString(R.string.offline_error)
            override val unknownErrorMessage: String
                get() = context.getString(R.string.unknown_error)
        }
}