package com.gojek.assignment.github.trending.core.api

import com.gojek.assignment.github.trending.data.model.GithubRepo
import io.reactivex.Single
import retrofit2.http.GET

interface GithubTrendingApi {
    @GET("repositories")
    fun fetchRepositories(): Single<List<GithubRepo>>
}