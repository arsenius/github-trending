package com.gojek.assignment.github.trending.data.state

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.Uninitialized
import com.gojek.assignment.github.trending.data.model.GithubRepo

/*
if in future implementation requires pagination we can easily reuse this data structure
    implementation without struggles
 */

data class GithubTrendingState (
    val repos: List<GithubRepo> = emptyList(),
    val reposRequest: Pair<LoadingType, Async<List<GithubRepo>>> = LoadingType.None to Uninitialized,
    val mainErrorMessage: String = "",
    val expandedIndex: Int? = null
): MvRxState {
    enum class LoadingType {
        None,
        Reloading,
        Completed
    }
}