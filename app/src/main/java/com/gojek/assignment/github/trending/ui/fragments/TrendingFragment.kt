package com.gojek.assignment.github.trending.ui.fragments


import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState

import com.gojek.assignment.github.trending.R
import com.gojek.assignment.github.trending.core.extensions.currentScrollPosition
import com.gojek.assignment.github.trending.core.extensions.isScrollEnabled
import com.gojek.assignment.github.trending.core.extensions.onScrollChangedState
import com.gojek.assignment.github.trending.core.resources.MenuManager
import com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel
import com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel
import com.gojek.assignment.github.trending.data.state.GithubTrendingState
import com.gojek.assignment.github.trending.ui.controllers.GithubTrendingReposController
import kotlinx.android.synthetic.main.fragment_content.*
import kotlinx.android.synthetic.main.toolbar_main.*


/**
 * A simple [Fragment] subclass.
 *
 */
class TrendingFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener,
    MenuManager {

    private val trendingViewModel: GitHubTrendingViewModel by fragmentViewModel()
    private val uiViewModel: BasicUiViewModel by fragmentViewModel()

    private lateinit var controller: GithubTrendingReposController

    override val menuResId: Int = R.menu.main_menu

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_content, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refresher.setOnRefreshListener(this)
        setupActionBar(toolbar)
        initController()
        manageScroll()
        loadGithubRepos()
    }

    override fun invalidate() {
        controller.requestModelBuild()
        renderUi()
    }

    override fun onRefresh() {
        refresher.isRefreshing = false
        trendingViewModel.reloadRepos()
    }

    override fun onMenuItemSelected(menuId: Int) {
        when(menuId) {
            R.id.action_sort_by_stars -> trendingViewModel.sortBy(GitHubTrendingViewModel.Sort.ByStars)
            R.id.action_sort_by_name -> trendingViewModel.sortBy(GitHubTrendingViewModel.Sort.ByName)
        }
    }


    private fun initController() {
        controller = GithubTrendingReposController(trendingViewModel)
        controller.setOnDescriptionClick {
            launchUrl(it)
        }
        controller.setOnRefresh {
            onRefresh()
        }
        body.setController(controller)
    }

    private fun manageScroll() {
        body.onScrollChangedState { isScrollableToBottom, isScrollableToTop ->
            uiViewModel.onScrollStateChanged(isScrollableToBottom, isScrollableToTop)
        }
        scrollToTopButton.setOnClickListener {
                uiViewModel.scrollToTop()
        }
    }

    private fun loadGithubRepos() = withState(trendingViewModel){
        if(it.repos.isEmpty())
            onRefresh()
    }

    private fun renderUi() = withState(trendingViewModel, uiViewModel) {viewModel, ui ->
        val isLoading = viewModel.reposRequest.first == GithubTrendingState.LoadingType.Reloading

        refresher.isEnabled = !isLoading
        body.isScrollEnabled = !isLoading

        if(ui.canScroll && !isLoading)
            scrollToTopButton.show()
        else
            scrollToTopButton.hide()

        if(ui.scrollToPosition)
            body.smoothScrollToPosition(ui.scrollingPosition)

        uiViewModel.setCurrentScrollPosition(body.currentScrollPosition)
    }



    private fun launchUrl(url: String) =
        CustomTabsIntent
            .Builder()
            .addDefaultShareMenuItem()
            .setToolbarColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
            .build()
            .launchUrl(requireContext(), Uri.parse(url))

}

