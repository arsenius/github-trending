

@EpoxyDataBindingLayouts({R.layout.row_main_error,
        R.layout.row_github_repo,
        R.layout.row_empty,
        R.layout.row_github_repo_loading})
package com.gojek.assignment.github.trending;

import com.airbnb.epoxy.EpoxyDataBindingLayouts;