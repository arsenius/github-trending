package com.gojek.assignment.github.trending

import androidx.multidex.MultiDexApplication
import com.airbnb.epoxy.EpoxyAsyncUtil
import com.airbnb.epoxy.EpoxyController
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.gojek.assignment.github.trending.core.di.Modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import android.content.Intent
import android.os.Handler
import androidx.core.os.bundleOf
import com.google.android.gms.security.ProviderInstaller.ProviderInstallListener
import com.google.android.gms.security.ProviderInstaller
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import androidx.localbroadcastmanager.content.LocalBroadcastManager



class App : MultiDexApplication() {

    private lateinit var broadcaster: LocalBroadcastManager
    private lateinit var googleApi: GoogleApiAvailability

    override fun onCreate() {
        super.onCreate()
        upgradeSecurityProvider()
        initDependencyInjection()
        initFresco()
        setupEpoxy()
    }

    private fun initDependencyInjection() {
        startKoin {
            androidContext(this@App)
            modules(Modules(this@App))
        }
    }

    private fun initFresco() {

        Fresco.initialize(
            this,
            ImagePipelineConfig
                .newBuilder(this)
                .setDownsampleEnabled(true)
                .build()
        )
    }

    private fun setupEpoxy() {
        val handler = EpoxyAsyncUtil.getAsyncBackgroundHandler()
        EpoxyController.defaultDiffingHandler = handler
        EpoxyController.defaultModelBuildingHandler = handler
    }

    private fun checkPlayServices(): Boolean {
        googleApi = GoogleApiAvailability.getInstance()
        val result = googleApi.isGooglePlayServicesAvailable(this)
        if (result != ConnectionResult.SUCCESS) {
            broadcastIntent(result)
            return false
        }

        return true
    }



    private fun upgradeSecurityProvider() {
        broadcaster = LocalBroadcastManager.getInstance(this)
        if(checkPlayServices())
            ProviderInstaller.installIfNeededAsync(this, object : ProviderInstallListener {
                override fun onProviderInstalled() {

                }

                override fun onProviderInstallFailed(errorCode: Int, recoveryIntent: Intent) {
                        broadcastIntent(errorCode)
                }
            })
    }

    private fun resolveGoogleApiError(errorCode: Int, resolved: () -> Unit) {
        if(googleApi.isUserResolvableError(errorCode))
            resolved()
    }

    private fun broadcastIntent(errorCode: Int) {
        resolveGoogleApiError(errorCode) {
            Handler().postDelayed({
                broadcaster.sendBroadcast(Intent("gms_data").apply {
                    putExtras(bundleOf("error_code" to errorCode))
                })
            }, 500)
        }
    }
}