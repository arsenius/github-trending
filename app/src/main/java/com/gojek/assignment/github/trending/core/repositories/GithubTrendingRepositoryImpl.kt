package com.gojek.assignment.github.trending.core.repositories

import com.gojek.assignment.github.trending.core.api.GithubTrendingApi
import com.gojek.assignment.github.trending.core.retrofit.ApiContext

/*
    This is just outline for scalable architecture
 */

class GithubTrendingRepositoryImpl(
    private val context: ApiContext,
    private val api: GithubTrendingApi) : GithubTrendingRepository {

    override val isNetworkAvailable: Boolean
        get() = context.isNetworkAvailable

    override val errorMessages: ApiContext.ErrorMessages
        get() = context.errorMessages

    override fun retrieveRepositories() = api.fetchRepositories()

}