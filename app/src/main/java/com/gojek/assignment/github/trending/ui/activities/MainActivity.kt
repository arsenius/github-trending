package com.gojek.assignment.github.trending.ui.activities


import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.gojek.assignment.github.trending.core.resources.MenuManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.gojek.assignment.github.trending.R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var hasMenu = false

        passToFragment<MenuManager> {
            menuInflater.inflate(menuResId, menu)
            hasMenu = true
            true
        }

        return hasMenu
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        passToFragment<MenuManager> {
            if(item?.itemId != null)
                onMenuItemSelected(item.itemId)
            false
        }
        return super.onOptionsItemSelected(item)
    }

    private inline fun <reified T> passToFragment(fragment: T.() -> Boolean) {
        run loop@{
            navHostFragment.childFragmentManager.fragments.forEach {
                if (it is T)
                    if(fragment(it))
                        return@loop
            }
        }
    }

}
