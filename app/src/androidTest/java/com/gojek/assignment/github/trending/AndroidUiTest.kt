package com.gojek.assignment.github.trending

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.airbnb.epoxy.EpoxyRecyclerView
import com.gojek.assignment.github.trending.ui.activities.MainActivity
import org.awaitility.kotlin.await
import org.junit.Assert.assertEquals
import org.junit.Before


import org.junit.runner.RunWith


import org.junit.Rule
import org.junit.Test
import test.BaseTests
import java.util.concurrent.TimeUnit

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class AndroidUiTest: BaseTests() {

    val shrtAwait = 600L
    val awaitTime = 1000L
    val holdingActivity = 2000L

    private val context: Context by lazy {
        InstrumentationRegistry.getInstrumentation().context
    }

    @get:Rule
    open val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java,
        true,
        false
    )

    @Before
    fun setupTest() {
        setupKoin(context)
    }


    @Test
    fun loadingUiTest_success() {
        setupServer()
        basicActivityTestScope {
            activityRule.activity.findViewById<View>(R.id.skeletonView) != null
        }
    }

    @Test
    fun loadedUiTest_success() {
        setupServer()
        basicAwaitActivityTestScope {
            activityRule.activity.findViewById<View>(R.id.githubRepoView) != null
        }
    }

    @Test
    fun loadedUiTest_fail1() {
        setupEmptyServer()
        basicAwaitActivityTestScope {
            activityRule.activity.findViewById<View>(R.id.errorView) != null
        }
    }

    @Test
    fun loadedUiTest_fail2() {
        setupBrokenServer()
        basicAwaitActivityTestScope {
            activityRule.activity.findViewById<View>(R.id.errorView) != null
        }
    }

    @Test
    fun scrollUpButtonInitialState_success() {
        setupServer()
        basicActivityTestScope {
            onView(withId(R.id.scrollToTopButton)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)))
        }
    }

    @Test
    fun scrollUpButtonVisibleState_success() {
        setupLongeResponseServer()
        basicActivityTestScope(shrtAwait) {
            onView(withId(R.id.body)).perform(ViewActions.swipeUp())
            onView(withId(R.id.scrollToTopButton)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        }
    }

    @Test
    fun scrollUpButtonHideState_success() {
        setupServer()
        basicActivityTestScope(shrtAwait) {
            onView(withId(R.id.body)).perform(ViewActions.swipeUp())
            onView(withId(R.id.scrollToTopButton)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)))
        }
    }


    @Test
    fun expandCollapseRecyclerViewViewItem1_success() {
        setupServer()
        basicActivityTestScope(shrtAwait) {
            onView(withId(R.id.body))
                .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
            hasVisibleViewAtItemPosition(0, R.id.expandedDivider)
            onView(withId(R.id.body))
                .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
            hasVisibleViewAtItemPosition(0, R.id.divider)
        }
    }

    @Test
    fun expandCollapseRecyclerViewViewItem2_success() {
        setupServer()
        basicActivityTestScope(shrtAwait) {
            onView(withId(R.id.body))
                .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
            hasVisibleViewAtItemPosition(0, R.id.expandedDivider)
            onView(withId(R.id.body))
                .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
            hasVisibleViewAtItemPosition(1, R.id.expandedDivider)
            hasVisibleViewAtItemPosition(0, R.id.divider)
        }
    }


    private fun basicAwaitActivityTestScope(wait: Long = awaitTime, scope: () -> Boolean) {
        activityRule.launchActivity(null)
        await.atMost(wait, TimeUnit.MILLISECONDS).until {
            scope()
        }
        Thread.sleep(holdingActivity)
        activityRule.finishActivity()
    }

    private fun basicActivityTestScope(wait: Long = 250, scope: () -> Unit) {
        activityRule.launchActivity(null)
        Thread.sleep(wait)
        scope()
        Thread.sleep(holdingActivity)
        activityRule.finishActivity()
    }

    private fun hasVisibleViewAtItemPosition(position: Int, viewId: Int) {
        val body = activityRule.activity.findViewById<EpoxyRecyclerView>(R.id.body)

        assertEquals(body.adapter != null, true)

        val view = body.findViewHolderForAdapterPosition(position)?.itemView

        assertEquals(view != null, true)

        val child = view!!.findViewById<View>(viewId)

        assertEquals(child != null, true)

        assertEquals(child.visibility == View.VISIBLE, true)
    }

    override fun getFileFromResources(fileName: String) =
        context.assets.open(fileName).bufferedReader().use { it.readText() }

}





