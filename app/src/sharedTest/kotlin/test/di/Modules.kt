package com.gojek.assignment.github.trending.di

import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepositoryImpl
import com.gojek.assignment.github.trending.core.retrofit.ApiBuilder
import com.gojek.assignment.github.trending.core.retrofit.ApiContext
import org.koin.dsl.module

fun Modules(context: ApiContext) = module {
    single<GithubTrendingRepository> { GithubTrendingRepositoryImpl(context, ApiBuilder(context).build()) }
}