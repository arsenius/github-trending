package test

import android.content.Context
import com.gojek.assignment.github.trending.di.Modules
import com.gojek.assignment.github.trending.mocks.MockApiContext
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.AutoCloseKoinTest
import java.util.concurrent.TimeUnit

abstract class BaseTests: AutoCloseKoinTest() {

    val delayTime = 500L

    private lateinit var server: MockWebServer
    private lateinit var baseUrl: String
    protected val apiContext = MockApiContext {
        baseUrl
    }

    protected fun setupKoin(context: Context? = null) {
        stopKoin()
        startKoin {
            if(context != null)
                androidContext(context)
            modules(Modules(apiContext))
        }
    }

    protected fun setupServer() {
        closeServer()
        server = MockWebServer()
        server.enqueue(MockResponse().setBodyDelay(delayTime, TimeUnit.MILLISECONDS).setBody(getFileFromResources("response.json")))
        server.start()
        baseUrl = server.url("/").toString()
    }

    protected fun setupLongeResponseServer() {
        closeServer()
        server = MockWebServer()
        server.enqueue(MockResponse().setBodyDelay(delayTime, TimeUnit.MILLISECONDS).setBody(getFileFromResources("long_response.json")))
        server.start()
        baseUrl = server.url("/").toString()
    }

    protected fun setupEmptyServer() {
        closeServer()
        server = MockWebServer()
        server.enqueue(MockResponse().setBodyDelay(delayTime, TimeUnit.MILLISECONDS).setBody("[]"))
        server.start()
        baseUrl = server.url("/").toString()

    }

    protected fun setupBrokenServer() {
        closeServer()
        server = MockWebServer()
        server.enqueue(MockResponse().setBodyDelay(delayTime, TimeUnit.MILLISECONDS).setResponseCode(500).setBody("[]"))
        server.start()
        baseUrl = server.url("/").toString()

    }

    protected fun closeServer() {
        if(::server.isInitialized)
            server.shutdown()
    }

    protected abstract fun getFileFromResources(file: String): String


}