package com.gojek.assignment.github.trending.mocks

import com.gojek.assignment.github.trending.core.retrofit.ApiContext

class MockApiContext(private val getBaseUrl: () -> String): ApiContext {

    private var networkAvailable: Boolean = true

    fun simulateOnline() {
        networkAvailable = true
    }

    fun simulateOffline() {
        networkAvailable = false
    }

    override val baseUrl: String
        get() = getBaseUrl()
    override val cacheDir: String
        get() = ""
    override val isNetworkAvailable: Boolean
        get() =  networkAvailable
    override val errorMessages: ApiContext.ErrorMessages
        get() = object: ApiContext.ErrorMessages {
            override val offlineErrorMessage: String
                get() = "Oops! It seems that you are offline. Please check your internet connection."
            override val unknownErrorMessage: String
                get() = "An alien most probably blocking your signal."

        }

}