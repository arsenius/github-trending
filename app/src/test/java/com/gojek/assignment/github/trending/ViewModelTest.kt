package com.gojek.assignment.github.trending

import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.withState
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository
import com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel
import com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel
import com.gojek.assignment.github.trending.data.state.BasicUiState
import com.gojek.assignment.github.trending.data.state.GithubTrendingState
import org.junit.After
import org.junit.Test

import org.junit.Before
import org.awaitility.kotlin.await
import org.koin.test.inject
import test.BaseTests
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ViewModelTest: BaseTests() {

    val initTime = 150L
    val awaitTime = 1000L

    val expectedLoadedItemSize = 25

    private val mockRepository: GithubTrendingRepository by inject()

    private val mainViewModel: GitHubTrendingViewModel
        get() = GitHubTrendingViewModel(GithubTrendingState(), mockRepository)

    private val uiViewModel: BasicUiViewModel
        get() = BasicUiViewModel(BasicUiState())

    @Before
    fun setupTest() {
       setupKoin()
    }


    @Test
    fun loadingGithubRepos_success() {
        setupServer()
        val vm  = mainViewModel
        vm.reloadRepos()
        verifyWithPending {
            withState(vm) {
               it.reposRequest.first == GithubTrendingState.LoadingType.Reloading &&
                        it.reposRequest.second is Loading
            }
        }

        verifyWithPending(awaitTime)  {
            withState(vm) {
                it.reposRequest.first == GithubTrendingState.LoadingType.Completed &&
                        it.reposRequest.second is Success && it.repos.size == expectedLoadedItemSize
            }
        }

    }

    @Test
    fun loadingGithubReposEmpty_test() {
        setupEmptyServer()
        val vm  = mainViewModel
        vm.reloadRepos()

        verifyWithPending {
            withState(vm) {
                it.reposRequest.first == GithubTrendingState.LoadingType.Reloading &&
                        it.reposRequest.second is Loading
            }
        }

        verifyWithPending(awaitTime)  {
            withState(vm) {
                it.reposRequest.first == GithubTrendingState.LoadingType.Completed &&
                        it.reposRequest.second is Success && it.repos.isEmpty() &&
                        it.mainErrorMessage == apiContext.errorMessages.unknownErrorMessage
            }
        }
    }

    @Test
    fun loadingGithubRepos_offline() {
        setupBrokenServer()
        val vm  = mainViewModel
        apiContext.simulateOffline()
        vm.reloadRepos()
        verifyWithPending {
            withState(vm) {
                it.reposRequest.first == GithubTrendingState.LoadingType.Reloading &&
                        it.reposRequest.second is Loading
            }
        }

        verifyWithPending(awaitTime) {
            withState(vm) {
                it.reposRequest.first == GithubTrendingState.LoadingType.Completed &&
                        it.reposRequest.second is Fail &&
                        it.mainErrorMessage == apiContext.errorMessages.offlineErrorMessage
            }

        }
        apiContext.simulateOnline()
        setupServer()
    }

    @Test
    fun sortingReposByStars_success() {
        setupServer()
        val vm  = makeSureMainModelIsComplete(mainViewModel)
        vm.sortBy(GitHubTrendingViewModel.Sort.ByStars)
        verifyWithPending(awaitTime) {
            withState(vm){
                var success = true
                var previousValue = Int.MAX_VALUE
                for(repo in it.repos) {
                    if(repo.stars.toInt() > previousValue)
                        success = false
                    previousValue = repo.stars.toInt()
                }
                success
            }
        }
    }

    @Test
    fun sortingReposByName_success() {
        setupServer()
        val vm  = makeSureMainModelIsComplete(mainViewModel)
        vm.sortBy(GitHubTrendingViewModel.Sort.ByName)
        verifyWithPending(awaitTime) {
            withState(vm) {
                var success = true
                var previousValue = ""
                for (repo in it.repos) {
                    if (repo.name.toLowerCase() < previousValue)
                        success = false
                    previousValue = repo.name.toLowerCase()
                }
                success
            }
        }
    }

    @Test
    fun testExpandCollapseItems_success() {
        setupServer()
        val vm  = makeSureMainModelIsComplete(mainViewModel)
        vm.expandItem(0)
        verifyWithPending {
            withState(vm) {
                it.expandedIndex == 0
            }
        }
        vm.expandItem(0)
        verifyWithPending {
            withState(vm) {
                it.expandedIndex == null
            }
        }

        withState(vm) { state ->
            val lstIndex = state.repos.size-1
            vm.expandItem(lstIndex)
            verifyWithPending {
                withState(vm) { state ->
                    state.expandedIndex == lstIndex
                }
            }
        }
    }

    @Test
    fun loadingGithubRepos_fail() {
        setupBrokenServer()
        val vm  = mainViewModel
        vm.reloadRepos()
        verifyWithPending {
            withState(vm) {
                    it.reposRequest.first == GithubTrendingState.LoadingType.Reloading &&
                            it.reposRequest.second is Loading
            }
        }

        verifyWithPending(awaitTime) {
            withState(vm){
                it.reposRequest.first == GithubTrendingState.LoadingType.Completed &&
                        it.reposRequest.second is Fail
            }

        }
        setupServer()
    }

    @Test
    fun uiScrollState_test() {
        val vm = uiViewModel
        vm.onScrollStateChanged(isScrollableToBottom = false, isScrollableToTop = false)
        verifyWithPending {
            withState(vm) {
                !it.canScroll
            }
        }

        vm.onScrollStateChanged(isScrollableToBottom = true, isScrollableToTop = false)
        verifyWithPending {
            withState(vm) {
                !it.canScroll
            }
        }

        vm.onScrollStateChanged(isScrollableToBottom = false, isScrollableToTop = true)
        verifyWithPending {
            withState(vm) {
                !it.canScroll
            }
        }
        vm.onScrollStateChanged(isScrollableToBottom = true, isScrollableToTop = true)
        verifyWithPending {
            withState(vm) {
                it.canScroll
            }
        }
    }

    @Test
    fun uiScrollToTop_test() {
        val vm = uiViewModel
        vm.onScrollStateChanged(isScrollableToBottom = true, isScrollableToTop = true)
        verifyWithPending {
            withState(vm) {
                it.canScroll && !it.scrollToPosition
            }
        }
        vm.scrollToTop()
        verifyWithPending {
            withState(vm) {
                it.canScroll && it.scrollToPosition
            }
        }
    }

    @Test
    fun uiScrollPosition_test0() {
        val vm = uiViewModel
        runScrollPositionTest(vm)
        vm.setCurrentScrollPosition(0)
        verifyWithPending {
            withState(vm) {
                !it.canScroll && !it.scrollToPosition
            }
        }
    }

    @Test
    fun uiScrollPosition_test1() {
        val vm = uiViewModel
        runScrollPositionTest(vm)
        vm.setCurrentScrollPosition(-1)
        verifyWithPending {
            withState(vm) {
                !it.canScroll && !it.scrollToPosition
            }
        }
    }



    @After
    fun closeTest() {
        closeServer()
    }


    private fun runScrollPositionTest(vm: BasicUiViewModel) {
        vm.onScrollStateChanged(isScrollableToBottom = true, isScrollableToTop = true)
        verifyWithPending {
            withState(vm) {
                println(it)
                it.canScroll && !it.scrollToPosition
            }
        }
        vm.scrollToTop()
        verifyWithPending {
            withState(vm) {
                println(it)
                it.canScroll && it.scrollToPosition
            }
        }
        vm.setCurrentScrollPosition(1)
        verifyWithPending {
            withState(vm) {
                println(it)
                it.canScroll && it.scrollToPosition
            }
        }
    }


    private fun makeSureMainModelIsComplete(vm: GitHubTrendingViewModel): GitHubTrendingViewModel {
        val vm  = mainViewModel
        vm.reloadRepos()
        verifyWithPending(awaitTime) {
            withState(vm) {
                it.reposRequest.first == GithubTrendingState.LoadingType.Completed
            }
        }
        return vm
    }

    private fun verifyWithPending(awaitMili: Long = initTime,
                                condition: () -> Boolean) {

        await.atMost(awaitMili, TimeUnit.MILLISECONDS).until {
            condition()
        }
    }

    override fun getFileFromResources(fileName: String): String {
        val classLoader = javaClass.classLoader
        val resource = classLoader!!.getResource(fileName)
        return String(File(resource.path).readBytes())
    }


}
