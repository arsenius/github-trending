package com.gojek.assignment.github.trending.core.viewmodel;

import com.airbnb.mvrx.BaseMvRxViewModel;
import com.airbnb.mvrx.BuildConfig;
import com.airbnb.mvrx.MvRxState;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/gojek/assignment/github/trending/core/viewmodel/MvRxViewModel;", "S", "Lcom/airbnb/mvrx/MvRxState;", "Lcom/airbnb/mvrx/BaseMvRxViewModel;", "initialState", "(Lcom/airbnb/mvrx/MvRxState;)V", "app_debug"})
public abstract class MvRxViewModel<S extends com.airbnb.mvrx.MvRxState> extends com.airbnb.mvrx.BaseMvRxViewModel<S> {
    
    public MvRxViewModel(@org.jetbrains.annotations.NotNull()
    S initialState) {
        super(null, false, null);
    }
}