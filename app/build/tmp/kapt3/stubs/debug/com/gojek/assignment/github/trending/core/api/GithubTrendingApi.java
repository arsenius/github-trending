package com.gojek.assignment.github.trending.core.api;

import com.gojek.assignment.github.trending.data.model.GithubRepo;
import io.reactivex.Single;
import retrofit2.http.GET;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'\u00a8\u0006\u0006"}, d2 = {"Lcom/gojek/assignment/github/trending/core/api/GithubTrendingApi;", "", "fetchRepositories", "Lio/reactivex/Single;", "", "Lcom/gojek/assignment/github/trending/data/model/GithubRepo;", "app_debug"})
public abstract interface GithubTrendingApi {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "repositories")
    public abstract io.reactivex.Single<java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>> fetchRepositories();
}