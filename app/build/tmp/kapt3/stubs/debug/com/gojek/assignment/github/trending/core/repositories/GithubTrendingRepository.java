package com.gojek.assignment.github.trending.core.repositories;

import com.gojek.assignment.github.trending.core.retrofit.ApiContext;
import com.gojek.assignment.github.trending.data.model.GithubRepo;
import io.reactivex.Single;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/gojek/assignment/github/trending/core/repositories/GithubTrendingRepository;", "", "errorMessages", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "getErrorMessages", "()Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "isNetworkAvailable", "", "()Z", "retrieveRepositories", "Lio/reactivex/Single;", "", "Lcom/gojek/assignment/github/trending/data/model/GithubRepo;", "app_debug"})
public abstract interface GithubTrendingRepository {
    
    public abstract boolean isNetworkAvailable();
    
    @org.jetbrains.annotations.NotNull()
    public abstract io.reactivex.Single<java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>> retrieveRepositories();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.gojek.assignment.github.trending.core.retrofit.ApiContext.ErrorMessages getErrorMessages();
}