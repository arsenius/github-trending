package com.gojek.assignment.github.trending.core.retrofit;

import com.gojek.assignment.github.trending.core.retrofit.interceptors.InterceptorsProvider;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001e\u0010\u0011\u001a\n \u0013*\u0004\u0018\u0001H\u0012H\u0012\"\u0006\b\u0000\u0010\u0012\u0018\u0001H\u0086\b\u00a2\u0006\u0002\u0010\u0014R\u0014\u0010\u0005\u001a\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000e8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0015"}, d2 = {"Lcom/gojek/assignment/github/trending/core/retrofit/ApiBuilder;", "", "context", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;", "(Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;)V", "client", "Lokhttp3/OkHttpClient;", "getClient", "()Lokhttp3/OkHttpClient;", "moshi", "Lcom/squareup/moshi/Moshi;", "getMoshi", "()Lcom/squareup/moshi/Moshi;", "retrofit", "Lretrofit2/Retrofit;", "getRetrofit", "()Lretrofit2/Retrofit;", "build", "T", "kotlin.jvm.PlatformType", "()Ljava/lang/Object;", "app_debug"})
public final class ApiBuilder {
    private final com.gojek.assignment.github.trending.core.retrofit.ApiContext context = null;
    
    private final com.squareup.moshi.Moshi getMoshi() {
        return null;
    }
    
    private final okhttp3.OkHttpClient getClient() {
        return null;
    }
    
    private final retrofit2.Retrofit getRetrofit() {
        return null;
    }
    
    @kotlin.Suppress(names = {"NON_PUBLIC_CALL_FROM_PUBLIC_INLINE"})
    private final <T extends java.lang.Object>T build() {
        return null;
    }
    
    public ApiBuilder(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.retrofit.ApiContext context) {
        super();
    }
}