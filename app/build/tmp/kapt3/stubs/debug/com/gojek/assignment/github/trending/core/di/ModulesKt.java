package com.gojek.assignment.github.trending.core.di;

import android.content.Context;
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository;
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepositoryImpl;
import com.gojek.assignment.github.trending.core.retrofit.ApiBuilder;
import com.gojek.assignment.github.trending.core.retrofit.ApiContextImpl;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u00a8\u0006\u0004"}, d2 = {"Modules", "Lorg/koin/core/module/Module;", "context", "Landroid/content/Context;", "app_debug"})
public final class ModulesKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final org.koin.core.module.Module Modules(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
}