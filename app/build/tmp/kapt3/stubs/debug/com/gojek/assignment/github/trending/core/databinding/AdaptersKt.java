package com.gojek.assignment.github.trending.core.databinding;

import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import androidx.databinding.BindingAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.gojek.assignment.github.trending.R;
import java.lang.Exception;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u001a\u001a\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0005H\u0007\u00a8\u0006\t"}, d2 = {"setImageUrl", "", "view", "Lcom/facebook/drawee/view/SimpleDraweeView;", "imageUrl", "", "setShapeDrawableBgColor", "Landroid/view/View;", "hexColor", "app_debug"})
public final class AdaptersKt {
    
    @androidx.databinding.BindingAdapter(value = {"imageUrl"})
    public static final void setImageUrl(@org.jetbrains.annotations.NotNull()
    com.facebook.drawee.view.SimpleDraweeView view, @org.jetbrains.annotations.NotNull()
    java.lang.String imageUrl) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"shapeBgColor"})
    public static final void setShapeDrawableBgColor(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    java.lang.String hexColor) {
    }
}