package com.gojek.assignment.github.trending.core.extensions;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u00000\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001aB\u0010\u0012\u001a\u00020\u0013*\u00020\u000226\u0010\u0014\u001a2\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u0016\u0012\b\b\u0017\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u0016\u0012\b\b\u0017\u0012\u0004\b\b(\u0019\u0012\u0004\u0012\u00020\u00130\u0015\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"(\u0010\u0007\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\"(\u0010\f\u001a\u00020\u0006*\u00020\r2\u0006\u0010\u000b\u001a\u00020\u00068F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001a"}, d2 = {"currentScrollPosition", "", "Landroidx/recyclerview/widget/RecyclerView;", "getCurrentScrollPosition", "(Landroidx/recyclerview/widget/RecyclerView;)I", "enabled", "", "isScrollEnabled", "(Landroidx/recyclerview/widget/RecyclerView;)Z", "setScrollEnabled", "(Landroidx/recyclerview/widget/RecyclerView;Z)V", "rounded", "roundedAsCircle", "Lcom/facebook/drawee/view/SimpleDraweeView;", "getRoundedAsCircle", "(Lcom/facebook/drawee/view/SimpleDraweeView;)Z", "setRoundedAsCircle", "(Lcom/facebook/drawee/view/SimpleDraweeView;Z)V", "onScrollChangedState", "", "onStateChanged", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "isScrollableToBottom", "isScrollableToTop", "app_debug"})
public final class RecyclerViewKt {
    
    public static final int getCurrentScrollPosition(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$currentScrollPosition) {
        return 0;
    }
    
    public static final boolean isScrollEnabled(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$isScrollEnabled) {
        return false;
    }
    
    public static final void setScrollEnabled(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$isScrollEnabled, boolean enabled) {
    }
    
    public static final void onScrollChangedState(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView $this$onScrollChangedState, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super java.lang.Boolean, ? super java.lang.Boolean, kotlin.Unit> onStateChanged) {
    }
    
    public static final boolean getRoundedAsCircle(@org.jetbrains.annotations.NotNull()
    com.facebook.drawee.view.SimpleDraweeView $this$roundedAsCircle) {
        return false;
    }
    
    public static final void setRoundedAsCircle(@org.jetbrains.annotations.NotNull()
    com.facebook.drawee.view.SimpleDraweeView $this$roundedAsCircle, boolean rounded) {
    }
}