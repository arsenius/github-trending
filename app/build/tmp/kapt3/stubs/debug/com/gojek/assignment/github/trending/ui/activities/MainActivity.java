package com.gojek.assignment.github.trending.ui.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.gojek.assignment.github.trending.core.resources.MenuManager;
import kotlinx.android.synthetic.main.activity_main.*;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0012\u0010\u000b\u001a\u00020\b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J*\u0010\u000e\u001a\u00020\u0004\"\u0006\b\u0000\u0010\u000f\u0018\u00012\u0017\u0010\u0010\u001a\u0013\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\b0\u0011\u00a2\u0006\u0002\b\u0012H\u0082\b\u00a8\u0006\u0013"}, d2 = {"Lcom/gojek/assignment/github/trending/ui/activities/MainActivity;", "Lcom/gojek/assignment/github/trending/ui/activities/BaseActivity;", "()V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "passToFragment", "T", "fragment", "Lkotlin/Function1;", "Lkotlin/ExtensionFunctionType;", "app_debug"})
public final class MainActivity extends com.gojek.assignment.github.trending.ui.activities.BaseActivity {
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.Nullable()
    android.view.MenuItem item) {
        return false;
    }
    
    private final <T extends java.lang.Object>void passToFragment(kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> fragment) {
    }
    
    public MainActivity() {
        super();
    }
}