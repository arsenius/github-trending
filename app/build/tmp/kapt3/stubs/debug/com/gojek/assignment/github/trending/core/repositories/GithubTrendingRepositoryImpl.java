package com.gojek.assignment.github.trending.core.repositories;

import com.gojek.assignment.github.trending.core.api.GithubTrendingApi;
import com.gojek.assignment.github.trending.core.retrofit.ApiContext;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00100\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\r\u00a8\u0006\u0012"}, d2 = {"Lcom/gojek/assignment/github/trending/core/repositories/GithubTrendingRepositoryImpl;", "Lcom/gojek/assignment/github/trending/core/repositories/GithubTrendingRepository;", "context", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;", "api", "Lcom/gojek/assignment/github/trending/core/api/GithubTrendingApi;", "(Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;Lcom/gojek/assignment/github/trending/core/api/GithubTrendingApi;)V", "errorMessages", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "getErrorMessages", "()Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "isNetworkAvailable", "", "()Z", "retrieveRepositories", "Lio/reactivex/Single;", "", "Lcom/gojek/assignment/github/trending/data/model/GithubRepo;", "app_debug"})
public final class GithubTrendingRepositoryImpl implements com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository {
    private final com.gojek.assignment.github.trending.core.retrofit.ApiContext context = null;
    private final com.gojek.assignment.github.trending.core.api.GithubTrendingApi api = null;
    
    @java.lang.Override()
    public boolean isNetworkAvailable() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.gojek.assignment.github.trending.core.retrofit.ApiContext.ErrorMessages getErrorMessages() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>> retrieveRepositories() {
        return null;
    }
    
    public GithubTrendingRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.retrofit.ApiContext context, @org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.api.GithubTrendingApi api) {
        super();
    }
}