package com.gojek.assignment.github.trending.core.retrofit;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001:\u0001\u000fR\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005R\u0012\u0010\b\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0012\u0010\f\u001a\u00020\rX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\u000e\u00a8\u0006\u0010"}, d2 = {"Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;", "", "baseUrl", "", "getBaseUrl", "()Ljava/lang/String;", "cacheDir", "getCacheDir", "errorMessages", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "getErrorMessages", "()Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "isNetworkAvailable", "", "()Z", "ErrorMessages", "app_debug"})
public abstract interface ApiContext {
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getBaseUrl();
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getCacheDir();
    
    public abstract boolean isNetworkAvailable();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.gojek.assignment.github.trending.core.retrofit.ApiContext.ErrorMessages getErrorMessages();
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005\u00a8\u0006\b"}, d2 = {"Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "", "offlineErrorMessage", "", "getOfflineErrorMessage", "()Ljava/lang/String;", "unknownErrorMessage", "getUnknownErrorMessage", "app_debug"})
    public static abstract interface ErrorMessages {
        
        @org.jetbrains.annotations.NotNull()
        public abstract java.lang.String getOfflineErrorMessage();
        
        @org.jetbrains.annotations.NotNull()
        public abstract java.lang.String getUnknownErrorMessage();
    }
}