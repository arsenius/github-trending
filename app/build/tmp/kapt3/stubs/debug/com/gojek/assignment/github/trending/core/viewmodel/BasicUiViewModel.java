package com.gojek.assignment.github.trending.core.viewmodel;

import com.airbnb.mvrx.MvRxViewModelFactory;
import com.airbnb.mvrx.ViewModelContext;
import com.gojek.assignment.github.trending.data.state.BasicUiState;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000eB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bJ\u0006\u0010\n\u001a\u00020\u0006J\u000e\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r\u00a8\u0006\u000f"}, d2 = {"Lcom/gojek/assignment/github/trending/core/viewmodel/BasicUiViewModel;", "Lcom/gojek/assignment/github/trending/core/viewmodel/MvRxViewModel;", "Lcom/gojek/assignment/github/trending/data/state/BasicUiState;", "initialState", "(Lcom/gojek/assignment/github/trending/data/state/BasicUiState;)V", "onScrollStateChanged", "", "isScrollableToBottom", "", "isScrollableToTop", "scrollToTop", "setCurrentScrollPosition", "position", "", "Companion", "app_debug"})
public final class BasicUiViewModel extends com.gojek.assignment.github.trending.core.viewmodel.MvRxViewModel<com.gojek.assignment.github.trending.data.state.BasicUiState> {
    public static final com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel.Companion Companion = null;
    
    public final void onScrollStateChanged(boolean isScrollableToBottom, boolean isScrollableToTop) {
    }
    
    public final void scrollToTop() {
    }
    
    public final void setCurrentScrollPosition(int position) {
    }
    
    public BasicUiViewModel(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.data.state.BasicUiState initialState) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0003H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/gojek/assignment/github/trending/core/viewmodel/BasicUiViewModel$Companion;", "Lcom/airbnb/mvrx/MvRxViewModelFactory;", "Lcom/gojek/assignment/github/trending/core/viewmodel/BasicUiViewModel;", "Lcom/gojek/assignment/github/trending/data/state/BasicUiState;", "()V", "create", "viewModelContext", "Lcom/airbnb/mvrx/ViewModelContext;", "state", "app_debug"})
    public static final class Companion implements com.airbnb.mvrx.MvRxViewModelFactory<com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel, com.gojek.assignment.github.trending.data.state.BasicUiState> {
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel create(@org.jetbrains.annotations.NotNull()
        com.airbnb.mvrx.ViewModelContext viewModelContext, @org.jetbrains.annotations.NotNull()
        com.gojek.assignment.github.trending.data.state.BasicUiState state) {
            return null;
        }
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.Nullable()
        public com.gojek.assignment.github.trending.data.state.BasicUiState initialState(@org.jetbrains.annotations.NotNull()
        com.airbnb.mvrx.ViewModelContext viewModelContext) {
            return null;
        }
    }
}