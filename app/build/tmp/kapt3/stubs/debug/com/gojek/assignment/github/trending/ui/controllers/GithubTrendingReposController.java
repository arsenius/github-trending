package com.gojek.assignment.github.trending.ui.controllers;

import com.airbnb.epoxy.EpoxyController;
import com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel;
import com.gojek.assignment.github.trending.data.model.GithubRepo;
import com.gojek.assignment.github.trending.data.state.GithubTrendingState;
import com.gojek.assignment.github.trending.rowEmpty;
import com.gojek.assignment.github.trending.rowGithubRepo;
import com.gojek.assignment.github.trending.rowGithubRepoLoading;
import com.gojek.assignment.github.trending.rowMainError;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u00020\bH\u0014J%\u0010\f\u001a\u00020\b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0002\u00a2\u0006\u0002\u0010\u0012J\b\u0010\u0013\u001a\u00020\bH\u0002J)\u0010\u0014\u001a\u00020\b2!\u0010\u0015\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\f\b\u0016\u0012\b\b\u0017\u0012\u0004\b\b(\u0018\u0012\u0004\u0012\u00020\b0\u0006J\u0014\u0010\u0019\u001a\u00020\b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\b0\nR\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/gojek/assignment/github/trending/ui/controllers/GithubTrendingReposController;", "Lcom/airbnb/epoxy/EpoxyController;", "viewModel", "Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel;", "(Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel;)V", "onDescriptionClick", "Lkotlin/Function1;", "", "", "onRefresh", "Lkotlin/Function0;", "buildModels", "renderRepos", "repos", "", "Lcom/gojek/assignment/github/trending/data/model/GithubRepo;", "expandedIndex", "", "(Ljava/util/List;Ljava/lang/Integer;)V", "renderSkeleton", "setOnDescriptionClick", "listener", "Lkotlin/ParameterName;", "name", "url", "setOnRefresh", "app_debug"})
public final class GithubTrendingReposController extends com.airbnb.epoxy.EpoxyController {
    private kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onDescriptionClick;
    private kotlin.jvm.functions.Function0<kotlin.Unit> onRefresh;
    private final com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel viewModel = null;
    
    public final void setOnDescriptionClick(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> listener) {
    }
    
    public final void setOnRefresh(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> listener) {
    }
    
    @java.lang.Override()
    protected void buildModels() {
    }
    
    private final void renderSkeleton() {
    }
    
    private final void renderRepos(java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo> repos, java.lang.Integer expandedIndex) {
    }
    
    public GithubTrendingReposController(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel viewModel) {
        super();
    }
}