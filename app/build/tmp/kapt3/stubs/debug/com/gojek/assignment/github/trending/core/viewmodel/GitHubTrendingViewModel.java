package com.gojek.assignment.github.trending.core.viewmodel;

import com.airbnb.mvrx.*;
import com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository;
import com.gojek.assignment.github.trending.data.state.GithubTrendingState;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00132\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0013\u0014B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0006\u0010\u000f\u001a\u00020\bJ\u000e\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel;", "Lcom/gojek/assignment/github/trending/core/viewmodel/MvRxViewModel;", "Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState;", "initialState", "githubRepo", "Lcom/gojek/assignment/github/trending/core/repositories/GithubTrendingRepository;", "(Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState;Lcom/gojek/assignment/github/trending/core/repositories/GithubTrendingRepository;)V", "expandItem", "", "index", "", "getErrorMessage", "", "error", "", "reloadRepos", "sortBy", "sort", "Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel$Sort;", "Companion", "Sort", "app_debug"})
public final class GitHubTrendingViewModel extends com.gojek.assignment.github.trending.core.viewmodel.MvRxViewModel<com.gojek.assignment.github.trending.data.state.GithubTrendingState> {
    private final com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository githubRepo = null;
    public static final com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel.Companion Companion = null;
    
    public final void sortBy(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel.Sort sort) {
    }
    
    public final void expandItem(int index) {
    }
    
    public final void reloadRepos() {
    }
    
    private final java.lang.String getErrorMessage(java.lang.Throwable error) {
        return null;
    }
    
    public GitHubTrendingViewModel(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.data.state.GithubTrendingState initialState, @org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.repositories.GithubTrendingRepository githubRepo) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel$Sort;", "", "(Ljava/lang/String;I)V", "ByStars", "ByName", "app_debug"})
    public static enum Sort {
        /*public static final*/ ByStars /* = new ByStars() */,
        /*public static final*/ ByName /* = new ByName() */;
        
        Sort() {
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0003H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel$Companion;", "Lcom/airbnb/mvrx/MvRxViewModelFactory;", "Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel;", "Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState;", "()V", "create", "viewModelContext", "Lcom/airbnb/mvrx/ViewModelContext;", "state", "app_debug"})
    public static final class Companion implements com.airbnb.mvrx.MvRxViewModelFactory<com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel, com.gojek.assignment.github.trending.data.state.GithubTrendingState> {
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel create(@org.jetbrains.annotations.NotNull()
        com.airbnb.mvrx.ViewModelContext viewModelContext, @org.jetbrains.annotations.NotNull()
        com.gojek.assignment.github.trending.data.state.GithubTrendingState state) {
            return null;
        }
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.Nullable()
        public com.gojek.assignment.github.trending.data.state.GithubTrendingState initialState(@org.jetbrains.annotations.NotNull()
        com.airbnb.mvrx.ViewModelContext viewModelContext) {
            return null;
        }
    }
}