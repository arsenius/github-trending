package com.gojek.assignment.github.trending.ui.fragments;

import androidx.appcompat.widget.Toolbar;
import com.airbnb.mvrx.BaseMvRxActivity;
import com.airbnb.mvrx.BaseMvRxFragment;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0004R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lcom/gojek/assignment/github/trending/ui/fragments/BaseFragment;", "Lcom/airbnb/mvrx/BaseMvRxFragment;", "()V", "parentActivity", "Lcom/airbnb/mvrx/BaseMvRxActivity;", "getParentActivity", "()Lcom/airbnb/mvrx/BaseMvRxActivity;", "parentActivity$delegate", "Lkotlin/Lazy;", "setupActionBar", "", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "app_debug"})
public abstract class BaseFragment extends com.airbnb.mvrx.BaseMvRxFragment {
    private final kotlin.Lazy parentActivity$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.airbnb.mvrx.BaseMvRxActivity getParentActivity() {
        return null;
    }
    
    protected final void setupActionBar(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.widget.Toolbar toolbar) {
    }
    
    public BaseFragment() {
        super();
    }
}