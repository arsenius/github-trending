package com.gojek.assignment.github.trending.core.retrofit.interceptors;

import android.util.Log;
import com.gojek.assignment.github.trending.core.retrofit.ApiContext;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import java.io.File;
import java.util.concurrent.TimeUnit;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/gojek/assignment/github/trending/core/retrofit/interceptors/InterceptorsProvider;", "", "context", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;", "(Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;)V", "cache", "Lokhttp3/Cache;", "getCache", "()Lokhttp3/Cache;", "cacheExpirationInHours", "", "cacheSize", "", "headerCacheControl", "", "headerPragma", "httpCache", "offlineCacheInterceptor", "Lokhttp3/Interceptor;", "getOfflineCacheInterceptor", "()Lokhttp3/Interceptor;", "app_debug"})
public final class InterceptorsProvider {
    private final java.lang.String headerCacheControl = "Cache-Control";
    private final java.lang.String httpCache = "http-cache";
    private final java.lang.String headerPragma = "Pragma";
    private final long cacheSize = 15728640L;
    private final int cacheExpirationInHours = 2;
    private final com.gojek.assignment.github.trending.core.retrofit.ApiContext context = null;
    
    @org.jetbrains.annotations.Nullable()
    public final okhttp3.Cache getCache() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final okhttp3.Interceptor getOfflineCacheInterceptor() {
        return null;
    }
    
    public InterceptorsProvider(@org.jetbrains.annotations.NotNull()
    com.gojek.assignment.github.trending.core.retrofit.ApiContext context) {
        super();
    }
}