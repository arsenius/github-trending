package com.gojek.assignment.github.trending.core.retrofit;

import android.content.Context;
import android.net.ConnectivityManager;
import com.gojek.assignment.github.trending.R;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0011\u00a8\u0006\u0012"}, d2 = {"Lcom/gojek/assignment/github/trending/core/retrofit/ApiContextImpl;", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "baseUrl", "", "getBaseUrl", "()Ljava/lang/String;", "cacheDir", "getCacheDir", "errorMessages", "Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "getErrorMessages", "()Lcom/gojek/assignment/github/trending/core/retrofit/ApiContext$ErrorMessages;", "isNetworkAvailable", "", "()Z", "app_debug"})
public final class ApiContextImpl implements com.gojek.assignment.github.trending.core.retrofit.ApiContext {
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getBaseUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getCacheDir() {
        return null;
    }
    
    @java.lang.Override()
    public boolean isNetworkAvailable() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.gojek.assignment.github.trending.core.retrofit.ApiContext.ErrorMessages getErrorMessages() {
        return null;
    }
    
    public ApiContextImpl(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}