package com.gojek.assignment.github.trending;

import androidx.multidex.MultiDexApplication;
import com.airbnb.epoxy.EpoxyAsyncUtil;
import com.airbnb.epoxy.EpoxyController;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import android.content.Intent;
import android.os.Handler;
import com.google.android.gms.security.ProviderInstaller.ProviderInstallListener;
import com.google.android.gms.security.ProviderInstaller;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\b\u0010\u000b\u001a\u00020\fH\u0002J\b\u0010\r\u001a\u00020\bH\u0002J\b\u0010\u000e\u001a\u00020\bH\u0002J\b\u0010\u000f\u001a\u00020\bH\u0016J\u001e\u0010\u0010\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\b0\u0012H\u0002J\b\u0010\u0013\u001a\u00020\bH\u0002J\b\u0010\u0014\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/gojek/assignment/github/trending/App;", "Landroidx/multidex/MultiDexApplication;", "()V", "broadcaster", "Landroidx/localbroadcastmanager/content/LocalBroadcastManager;", "googleApi", "Lcom/google/android/gms/common/GoogleApiAvailability;", "broadcastIntent", "", "errorCode", "", "checkPlayServices", "", "initDependencyInjection", "initFresco", "onCreate", "resolveGoogleApiError", "resolved", "Lkotlin/Function0;", "setupEpoxy", "upgradeSecurityProvider", "app_debug"})
public final class App extends androidx.multidex.MultiDexApplication {
    private androidx.localbroadcastmanager.content.LocalBroadcastManager broadcaster;
    private com.google.android.gms.common.GoogleApiAvailability googleApi;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final void initDependencyInjection() {
    }
    
    private final void initFresco() {
    }
    
    private final void setupEpoxy() {
    }
    
    private final boolean checkPlayServices() {
        return false;
    }
    
    private final void upgradeSecurityProvider() {
    }
    
    private final void resolveGoogleApiError(int errorCode, kotlin.jvm.functions.Function0<kotlin.Unit> resolved) {
    }
    
    private final void broadcastIntent(int errorCode) {
    }
    
    public App() {
        super();
    }
}