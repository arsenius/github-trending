package com.gojek.assignment.github.trending.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.gojek.assignment.github.trending.R;
import com.gojek.assignment.github.trending.core.resources.MenuManager;
import com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel;
import com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel;
import com.gojek.assignment.github.trending.data.state.GithubTrendingState;
import com.gojek.assignment.github.trending.ui.controllers.GithubTrendingReposController;
import kotlinx.android.synthetic.main.fragment_content.*;
import kotlinx.android.synthetic.main.toolbar_main.*;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\b\u0010\u0018\u001a\u00020\u0017H\u0016J\u0010\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\b\u0010\u001c\u001a\u00020\u0017H\u0002J\b\u0010\u001d\u001a\u00020\u0017H\u0002J&\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0010\u0010&\u001a\u00020\u00172\u0006\u0010\'\u001a\u00020\bH\u0016J\b\u0010(\u001a\u00020\u0017H\u0016J\u001a\u0010)\u001a\u00020\u00172\u0006\u0010*\u001a\u00020\u001f2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\b\u0010+\u001a\u00020\u0017H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\bX\u0096D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0011\u001a\u00020\u00128BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0013\u0010\u0014\u00a8\u0006,"}, d2 = {"Lcom/gojek/assignment/github/trending/ui/fragments/TrendingFragment;", "Lcom/gojek/assignment/github/trending/ui/fragments/BaseFragment;", "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;", "Lcom/gojek/assignment/github/trending/core/resources/MenuManager;", "()V", "controller", "Lcom/gojek/assignment/github/trending/ui/controllers/GithubTrendingReposController;", "menuResId", "", "getMenuResId", "()I", "trendingViewModel", "Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel;", "getTrendingViewModel", "()Lcom/gojek/assignment/github/trending/core/viewmodel/GitHubTrendingViewModel;", "trendingViewModel$delegate", "Lcom/airbnb/mvrx/lifecycleAwareLazy;", "uiViewModel", "Lcom/gojek/assignment/github/trending/core/viewmodel/BasicUiViewModel;", "getUiViewModel", "()Lcom/gojek/assignment/github/trending/core/viewmodel/BasicUiViewModel;", "uiViewModel$delegate", "initController", "", "invalidate", "launchUrl", "url", "", "loadGithubRepos", "manageScroll", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onMenuItemSelected", "menuId", "onRefresh", "onViewCreated", "view", "renderUi", "app_debug"})
public final class TrendingFragment extends com.gojek.assignment.github.trending.ui.fragments.BaseFragment implements androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener, com.gojek.assignment.github.trending.core.resources.MenuManager {
    private final com.airbnb.mvrx.lifecycleAwareLazy trendingViewModel$delegate = null;
    private final com.airbnb.mvrx.lifecycleAwareLazy uiViewModel$delegate = null;
    private com.gojek.assignment.github.trending.ui.controllers.GithubTrendingReposController controller;
    private final int menuResId = com.gojek.assignment.github.trending.R.menu.main_menu;
    private java.util.HashMap _$_findViewCache;
    
    private final com.gojek.assignment.github.trending.core.viewmodel.GitHubTrendingViewModel getTrendingViewModel() {
        return null;
    }
    
    private final com.gojek.assignment.github.trending.core.viewmodel.BasicUiViewModel getUiViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public int getMenuResId() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void invalidate() {
    }
    
    @java.lang.Override()
    public void onRefresh() {
    }
    
    @java.lang.Override()
    public void onMenuItemSelected(int menuId) {
    }
    
    private final void initController() {
    }
    
    private final void manageScroll() {
    }
    
    private final void loadGithubRepos() {
    }
    
    private final void renderUi() {
    }
    
    private final void launchUrl(java.lang.String url) {
    }
    
    public TrendingFragment() {
        super();
    }
}