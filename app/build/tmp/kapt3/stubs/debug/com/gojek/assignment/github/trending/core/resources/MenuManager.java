package com.gojek.assignment.github.trending.core.resources;

import androidx.annotation.IdRes;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\u0003H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\t"}, d2 = {"Lcom/gojek/assignment/github/trending/core/resources/MenuManager;", "", "menuResId", "", "getMenuResId", "()I", "onMenuItemSelected", "", "menuId", "app_debug"})
public abstract interface MenuManager {
    
    public abstract int getMenuResId();
    
    public abstract void onMenuItemSelected(@androidx.annotation.IdRes()
    int menuId);
}