package com.gojek.assignment.github.trending.data.state;

import com.airbnb.mvrx.Async;
import com.airbnb.mvrx.MvRxState;
import com.airbnb.mvrx.Uninitialized;
import com.gojek.assignment.github.trending.data.model.GithubRepo;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001:\u0001#BM\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012 \b\u0002\u0010\u0005\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\b0\u0006\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\rJ\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J!\u0010\u0018\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\b0\u0006H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J\u0010\u0010\u001a\u001a\u0004\u0018\u00010\fH\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJV\u0010\u001b\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032 \b\u0002\u0010\u0005\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\b0\u00062\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00c6\u0001\u00a2\u0006\u0002\u0010\u001cJ\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\fH\u00d6\u0001J\t\u0010\"\u001a\u00020\nH\u00d6\u0001R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R)\u0010\u0005\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006$"}, d2 = {"Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState;", "Lcom/airbnb/mvrx/MvRxState;", "repos", "", "Lcom/gojek/assignment/github/trending/data/model/GithubRepo;", "reposRequest", "Lkotlin/Pair;", "Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState$LoadingType;", "Lcom/airbnb/mvrx/Async;", "mainErrorMessage", "", "expandedIndex", "", "(Ljava/util/List;Lkotlin/Pair;Ljava/lang/String;Ljava/lang/Integer;)V", "getExpandedIndex", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getMainErrorMessage", "()Ljava/lang/String;", "getRepos", "()Ljava/util/List;", "getReposRequest", "()Lkotlin/Pair;", "component1", "component2", "component3", "component4", "copy", "(Ljava/util/List;Lkotlin/Pair;Ljava/lang/String;Ljava/lang/Integer;)Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState;", "equals", "", "other", "", "hashCode", "toString", "LoadingType", "app_debug"})
public final class GithubTrendingState implements com.airbnb.mvrx.MvRxState {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo> repos = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Pair<com.gojek.assignment.github.trending.data.state.GithubTrendingState.LoadingType, com.airbnb.mvrx.Async<java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>>> reposRequest = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String mainErrorMessage = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer expandedIndex = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo> getRepos() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<com.gojek.assignment.github.trending.data.state.GithubTrendingState.LoadingType, com.airbnb.mvrx.Async<java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>>> getReposRequest() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMainErrorMessage() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getExpandedIndex() {
        return null;
    }
    
    public GithubTrendingState(@org.jetbrains.annotations.NotNull()
    java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo> repos, @org.jetbrains.annotations.NotNull()
    kotlin.Pair<? extends com.gojek.assignment.github.trending.data.state.GithubTrendingState.LoadingType, ? extends com.airbnb.mvrx.Async<? extends java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>>> reposRequest, @org.jetbrains.annotations.NotNull()
    java.lang.String mainErrorMessage, @org.jetbrains.annotations.Nullable()
    java.lang.Integer expandedIndex) {
        super();
    }
    
    public GithubTrendingState() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<com.gojek.assignment.github.trending.data.state.GithubTrendingState.LoadingType, com.airbnb.mvrx.Async<java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>>> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.gojek.assignment.github.trending.data.state.GithubTrendingState copy(@org.jetbrains.annotations.NotNull()
    java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo> repos, @org.jetbrains.annotations.NotNull()
    kotlin.Pair<? extends com.gojek.assignment.github.trending.data.state.GithubTrendingState.LoadingType, ? extends com.airbnb.mvrx.Async<? extends java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo>>> reposRequest, @org.jetbrains.annotations.NotNull()
    java.lang.String mainErrorMessage, @org.jetbrains.annotations.Nullable()
    java.lang.Integer expandedIndex) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/gojek/assignment/github/trending/data/state/GithubTrendingState$LoadingType;", "", "(Ljava/lang/String;I)V", "None", "Reloading", "Completed", "app_debug"})
    public static enum LoadingType {
        /*public static final*/ None /* = new None() */,
        /*public static final*/ Reloading /* = new Reloading() */,
        /*public static final*/ Completed /* = new Completed() */;
        
        LoadingType() {
        }
    }
}