package com.gojek.assignment.github.trending.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class RowMainErrorBinding extends ViewDataBinding {
  @NonNull
  public final ConstraintLayout errorView;

  @NonNull
  public final AppCompatImageView logo;

  @NonNull
  public final AppCompatTextView title;

  @Bindable
  protected String mError;

  @Bindable
  protected View.OnClickListener mOnRetryClick;

  protected RowMainErrorBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ConstraintLayout errorView, AppCompatImageView logo, AppCompatTextView title) {
    super(_bindingComponent, _root, _localFieldCount);
    this.errorView = errorView;
    this.logo = logo;
    this.title = title;
  }

  public abstract void setError(@Nullable String error);

  @Nullable
  public String getError() {
    return mError;
  }

  public abstract void setOnRetryClick(@Nullable View.OnClickListener onRetryClick);

  @Nullable
  public View.OnClickListener getOnRetryClick() {
    return mOnRetryClick;
  }

  @NonNull
  public static RowMainErrorBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_main_error, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static RowMainErrorBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<RowMainErrorBinding>inflateInternal(inflater, com.gojek.assignment.github.trending.R.layout.row_main_error, root, attachToRoot, component);
  }

  @NonNull
  public static RowMainErrorBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_main_error, null, false, component)
   */
  @NonNull
  @Deprecated
  public static RowMainErrorBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<RowMainErrorBinding>inflateInternal(inflater, com.gojek.assignment.github.trending.R.layout.row_main_error, null, false, component);
  }

  public static RowMainErrorBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static RowMainErrorBinding bind(@NonNull View view, @Nullable Object component) {
    return (RowMainErrorBinding)bind(component, view, com.gojek.assignment.github.trending.R.layout.row_main_error);
  }
}
