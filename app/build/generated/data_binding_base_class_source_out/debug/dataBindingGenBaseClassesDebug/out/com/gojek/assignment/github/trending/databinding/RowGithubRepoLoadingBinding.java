package com.gojek.assignment.github.trending.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class RowGithubRepoLoadingBinding extends ViewDataBinding {
  @NonNull
  public final View avatar;

  @NonNull
  public final View divider;

  @NonNull
  public final View line;

  @NonNull
  public final View line2;

  @NonNull
  public final ConstraintLayout skeletonView;

  protected RowGithubRepoLoadingBinding(Object _bindingComponent, View _root, int _localFieldCount,
      View avatar, View divider, View line, View line2, ConstraintLayout skeletonView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.avatar = avatar;
    this.divider = divider;
    this.line = line;
    this.line2 = line2;
    this.skeletonView = skeletonView;
  }

  @NonNull
  public static RowGithubRepoLoadingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_github_repo_loading, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static RowGithubRepoLoadingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<RowGithubRepoLoadingBinding>inflateInternal(inflater, com.gojek.assignment.github.trending.R.layout.row_github_repo_loading, root, attachToRoot, component);
  }

  @NonNull
  public static RowGithubRepoLoadingBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_github_repo_loading, null, false, component)
   */
  @NonNull
  @Deprecated
  public static RowGithubRepoLoadingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<RowGithubRepoLoadingBinding>inflateInternal(inflater, com.gojek.assignment.github.trending.R.layout.row_github_repo_loading, null, false, component);
  }

  public static RowGithubRepoLoadingBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static RowGithubRepoLoadingBinding bind(@NonNull View view, @Nullable Object component) {
    return (RowGithubRepoLoadingBinding)bind(component, view, com.gojek.assignment.github.trending.R.layout.row_github_repo_loading);
  }
}
