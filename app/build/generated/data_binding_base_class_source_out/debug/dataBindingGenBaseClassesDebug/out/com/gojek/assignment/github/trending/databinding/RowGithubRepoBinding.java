package com.gojek.assignment.github.trending.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.facebook.drawee.view.SimpleDraweeView;
import com.gojek.assignment.github.trending.data.model.GithubRepo;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class RowGithubRepoBinding extends ViewDataBinding {
  @NonNull
  public final AppCompatTextView author;

  @NonNull
  public final SimpleDraweeView avatar;

  @NonNull
  public final AppCompatTextView description;

  @NonNull
  public final View divider;

  @NonNull
  public final View expandedDivider;

  @NonNull
  public final AppCompatImageView fork;

  @NonNull
  public final AppCompatTextView forks;

  @NonNull
  public final ConstraintLayout githubRepoView;

  @NonNull
  public final AppCompatTextView language;

  @NonNull
  public final View languageColor;

  @NonNull
  public final AppCompatTextView name;

  @NonNull
  public final AppCompatImageView star;

  @NonNull
  public final AppCompatTextView stars;

  @Bindable
  protected GithubRepo mModel;

  @Bindable
  protected boolean mIsExpanded;

  @Bindable
  protected boolean mIsLastRow;

  @Bindable
  protected int mAvatarIndex;

  @Bindable
  protected View.OnClickListener mOnItemClick;

  @Bindable
  protected View.OnClickListener mOnDescriptionClick;

  protected RowGithubRepoBinding(Object _bindingComponent, View _root, int _localFieldCount,
      AppCompatTextView author, SimpleDraweeView avatar, AppCompatTextView description,
      View divider, View expandedDivider, AppCompatImageView fork, AppCompatTextView forks,
      ConstraintLayout githubRepoView, AppCompatTextView language, View languageColor,
      AppCompatTextView name, AppCompatImageView star, AppCompatTextView stars) {
    super(_bindingComponent, _root, _localFieldCount);
    this.author = author;
    this.avatar = avatar;
    this.description = description;
    this.divider = divider;
    this.expandedDivider = expandedDivider;
    this.fork = fork;
    this.forks = forks;
    this.githubRepoView = githubRepoView;
    this.language = language;
    this.languageColor = languageColor;
    this.name = name;
    this.star = star;
    this.stars = stars;
  }

  public abstract void setModel(@Nullable GithubRepo model);

  @Nullable
  public GithubRepo getModel() {
    return mModel;
  }

  public abstract void setIsExpanded(boolean isExpanded);

  public boolean getIsExpanded() {
    return mIsExpanded;
  }

  public abstract void setIsLastRow(boolean isLastRow);

  public boolean getIsLastRow() {
    return mIsLastRow;
  }

  public abstract void setAvatarIndex(int avatarIndex);

  public int getAvatarIndex() {
    return mAvatarIndex;
  }

  public abstract void setOnItemClick(@Nullable View.OnClickListener onItemClick);

  @Nullable
  public View.OnClickListener getOnItemClick() {
    return mOnItemClick;
  }

  public abstract void setOnDescriptionClick(@Nullable View.OnClickListener onDescriptionClick);

  @Nullable
  public View.OnClickListener getOnDescriptionClick() {
    return mOnDescriptionClick;
  }

  @NonNull
  public static RowGithubRepoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_github_repo, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static RowGithubRepoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<RowGithubRepoBinding>inflateInternal(inflater, com.gojek.assignment.github.trending.R.layout.row_github_repo, root, attachToRoot, component);
  }

  @NonNull
  public static RowGithubRepoBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_github_repo, null, false, component)
   */
  @NonNull
  @Deprecated
  public static RowGithubRepoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<RowGithubRepoBinding>inflateInternal(inflater, com.gojek.assignment.github.trending.R.layout.row_github_repo, null, false, component);
  }

  public static RowGithubRepoBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static RowGithubRepoBinding bind(@NonNull View view, @Nullable Object component) {
    return (RowGithubRepoBinding)bind(component, view, com.gojek.assignment.github.trending.R.layout.row_github_repo);
  }
}
