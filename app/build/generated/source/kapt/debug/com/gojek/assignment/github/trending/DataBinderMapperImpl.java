package com.gojek.assignment.github.trending;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.gojek.assignment.github.trending.databinding.RowEmptyBindingImpl;
import com.gojek.assignment.github.trending.databinding.RowGithubRepoBindingImpl;
import com.gojek.assignment.github.trending.databinding.RowGithubRepoLoadingBindingImpl;
import com.gojek.assignment.github.trending.databinding.RowMainErrorBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ROWEMPTY = 1;

  private static final int LAYOUT_ROWGITHUBREPO = 2;

  private static final int LAYOUT_ROWGITHUBREPOLOADING = 3;

  private static final int LAYOUT_ROWMAINERROR = 4;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(4);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.gojek.assignment.github.trending.R.layout.row_empty, LAYOUT_ROWEMPTY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.gojek.assignment.github.trending.R.layout.row_github_repo, LAYOUT_ROWGITHUBREPO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.gojek.assignment.github.trending.R.layout.row_github_repo_loading, LAYOUT_ROWGITHUBREPOLOADING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.gojek.assignment.github.trending.R.layout.row_main_error, LAYOUT_ROWMAINERROR);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ROWEMPTY: {
          if ("layout/row_empty_0".equals(tag)) {
            return new RowEmptyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for row_empty is invalid. Received: " + tag);
        }
        case  LAYOUT_ROWGITHUBREPO: {
          if ("layout/row_github_repo_0".equals(tag)) {
            return new RowGithubRepoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for row_github_repo is invalid. Received: " + tag);
        }
        case  LAYOUT_ROWGITHUBREPOLOADING: {
          if ("layout/row_github_repo_loading_0".equals(tag)) {
            return new RowGithubRepoLoadingBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for row_github_repo_loading is invalid. Received: " + tag);
        }
        case  LAYOUT_ROWMAINERROR: {
          if ("layout/row_main_error_0".equals(tag)) {
            return new RowMainErrorBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for row_main_error is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(2);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    result.add(new com.airbnb.epoxy.databinding.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(10);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "isExpanded");
      sKeys.put(2, "onRetryClick");
      sKeys.put(3, "onDescriptionClick");
      sKeys.put(4, "isLastRow");
      sKeys.put(5, "model");
      sKeys.put(6, "onItemClick");
      sKeys.put(7, "avatarIndex");
      sKeys.put(8, "error");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(4);

    static {
      sKeys.put("layout/row_empty_0", com.gojek.assignment.github.trending.R.layout.row_empty);
      sKeys.put("layout/row_github_repo_0", com.gojek.assignment.github.trending.R.layout.row_github_repo);
      sKeys.put("layout/row_github_repo_loading_0", com.gojek.assignment.github.trending.R.layout.row_github_repo_loading);
      sKeys.put("layout/row_main_error_0", com.gojek.assignment.github.trending.R.layout.row_main_error);
    }
  }
}
