package com.gojek.assignment.github.trending;

import android.view.View;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.OnModelBoundListener;
import com.airbnb.epoxy.OnModelClickListener;
import com.airbnb.epoxy.OnModelUnboundListener;
import com.airbnb.epoxy.OnModelVisibilityChangedListener;
import com.airbnb.epoxy.OnModelVisibilityStateChangedListener;
import com.gojek.assignment.github.trending.data.model.GithubRepo;
import java.lang.CharSequence;
import java.lang.Number;

public interface RowGithubRepoBindingModelBuilder {
  RowGithubRepoBindingModelBuilder onBind(
      OnModelBoundListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoBindingModelBuilder onUnbind(
      OnModelUnboundListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoBindingModelBuilder onVisibilityStateChanged(
      OnModelVisibilityStateChangedListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoBindingModelBuilder onVisibilityChanged(
      OnModelVisibilityChangedListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoBindingModelBuilder model(GithubRepo model);

  RowGithubRepoBindingModelBuilder isExpanded(boolean isExpanded);

  RowGithubRepoBindingModelBuilder isLastRow(boolean isLastRow);

  RowGithubRepoBindingModelBuilder avatarIndex(int avatarIndex);

  RowGithubRepoBindingModelBuilder onItemClick(
      final OnModelClickListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onItemClick);

  RowGithubRepoBindingModelBuilder onItemClick(View.OnClickListener onItemClick);

  RowGithubRepoBindingModelBuilder onDescriptionClick(
      final OnModelClickListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onDescriptionClick);

  RowGithubRepoBindingModelBuilder onDescriptionClick(View.OnClickListener onDescriptionClick);

  RowGithubRepoBindingModelBuilder id(long id);

  RowGithubRepoBindingModelBuilder id(@Nullable Number... arg0);

  RowGithubRepoBindingModelBuilder id(long id1, long id2);

  RowGithubRepoBindingModelBuilder id(@Nullable CharSequence arg0);

  RowGithubRepoBindingModelBuilder id(@Nullable CharSequence arg0, @Nullable CharSequence... arg1);

  RowGithubRepoBindingModelBuilder id(@Nullable CharSequence arg0, long arg1);

  RowGithubRepoBindingModelBuilder layout(@LayoutRes int arg0);

  RowGithubRepoBindingModelBuilder spanSizeOverride(
      @Nullable EpoxyModel.SpanSizeOverrideCallback arg0);
}
