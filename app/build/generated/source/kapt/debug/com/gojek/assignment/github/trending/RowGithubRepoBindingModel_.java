package com.gojek.assignment.github.trending;

import android.view.View;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyController;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.EpoxyViewHolder;
import com.airbnb.epoxy.GeneratedModel;
import com.airbnb.epoxy.OnModelBoundListener;
import com.airbnb.epoxy.OnModelClickListener;
import com.airbnb.epoxy.OnModelUnboundListener;
import com.airbnb.epoxy.OnModelVisibilityChangedListener;
import com.airbnb.epoxy.OnModelVisibilityStateChangedListener;
import com.airbnb.epoxy.WrappedEpoxyModelClickListener;
import com.gojek.assignment.github.trending.data.model.GithubRepo;
import java.lang.CharSequence;
import java.lang.IllegalStateException;
import java.lang.Number;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;

/**
 * Generated file. Do not modify! */
public class RowGithubRepoBindingModel_ extends DataBindingEpoxyModel implements GeneratedModel<DataBindingEpoxyModel.DataBindingHolder>, RowGithubRepoBindingModelBuilder {
  private OnModelBoundListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onModelBoundListener_epoxyGeneratedModel;

  private OnModelUnboundListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onModelUnboundListener_epoxyGeneratedModel;

  private OnModelVisibilityStateChangedListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onModelVisibilityStateChangedListener_epoxyGeneratedModel;

  private OnModelVisibilityChangedListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onModelVisibilityChangedListener_epoxyGeneratedModel;

  private GithubRepo model;

  private boolean isExpanded;

  private boolean isLastRow;

  private int avatarIndex;

  private View.OnClickListener onItemClick;

  private View.OnClickListener onDescriptionClick;

  @Override
  public void addTo(EpoxyController controller) {
    super.addTo(controller);
    addWithDebugValidation(controller);
  }

  @Override
  public void handlePreBind(final EpoxyViewHolder holder,
      final DataBindingEpoxyModel.DataBindingHolder object, final int position) {
    validateStateHasNotChangedSinceAdded("The model was changed between being added to the controller and being bound.", position);
  }

  @Override
  public void handlePostBind(final DataBindingEpoxyModel.DataBindingHolder object, int position) {
    if (onModelBoundListener_epoxyGeneratedModel != null) {
      onModelBoundListener_epoxyGeneratedModel.onModelBound(this, object, position);
    }
    validateStateHasNotChangedSinceAdded("The model was changed during the bind call.", position);
  }

  /**
   * Register a listener that will be called when this model is bound to a view.
   * <p>
   * The listener will contribute to this model's hashCode state per the {@link
   * com.airbnb.epoxy.EpoxyAttribute.Option#DoNotHash} rules.
   * <p>
   * You may clear the listener by setting a null value, or by calling {@link #reset()} */
  public RowGithubRepoBindingModel_ onBind(
      OnModelBoundListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener) {
    onMutation();
    this.onModelBoundListener_epoxyGeneratedModel = listener;
    return this;
  }

  @Override
  public void unbind(DataBindingEpoxyModel.DataBindingHolder object) {
    super.unbind(object);
    if (onModelUnboundListener_epoxyGeneratedModel != null) {
      onModelUnboundListener_epoxyGeneratedModel.onModelUnbound(this, object);
    }
  }

  /**
   * Register a listener that will be called when this model is unbound from a view.
   * <p>
   * The listener will contribute to this model's hashCode state per the {@link
   * com.airbnb.epoxy.EpoxyAttribute.Option#DoNotHash} rules.
   * <p>
   * You may clear the listener by setting a null value, or by calling {@link #reset()} */
  public RowGithubRepoBindingModel_ onUnbind(
      OnModelUnboundListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener) {
    onMutation();
    this.onModelUnboundListener_epoxyGeneratedModel = listener;
    return this;
  }

  @Override
  public void onVisibilityStateChanged(int visibilityState,
      final DataBindingEpoxyModel.DataBindingHolder object) {
    if (onModelVisibilityStateChangedListener_epoxyGeneratedModel != null) {
      onModelVisibilityStateChangedListener_epoxyGeneratedModel.onVisibilityStateChanged(this, object, visibilityState);
    }
    super.onVisibilityStateChanged(visibilityState, object);
  }

  /**
   * Register a listener that will be called when this model visibility state has changed.
   * <p>
   * The listener will contribute to this model's hashCode state per the {@link
   * com.airbnb.epoxy.EpoxyAttribute.Option#DoNotHash} rules.
   */
  public RowGithubRepoBindingModel_ onVisibilityStateChanged(
      OnModelVisibilityStateChangedListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener) {
    onMutation();
    this.onModelVisibilityStateChangedListener_epoxyGeneratedModel = listener;
    return this;
  }

  @Override
  public void onVisibilityChanged(float percentVisibleHeight, float percentVisibleWidth,
      int visibleHeight, int visibleWidth, final DataBindingEpoxyModel.DataBindingHolder object) {
    if (onModelVisibilityChangedListener_epoxyGeneratedModel != null) {
      onModelVisibilityChangedListener_epoxyGeneratedModel.onVisibilityChanged(this, object, percentVisibleHeight, percentVisibleWidth, visibleHeight, visibleWidth);
    }
    super.onVisibilityChanged(percentVisibleHeight, percentVisibleWidth, visibleHeight, visibleWidth, object);
  }

  /**
   * Register a listener that will be called when this model visibility has changed.
   * <p>
   * The listener will contribute to this model's hashCode state per the {@link
   * com.airbnb.epoxy.EpoxyAttribute.Option#DoNotHash} rules.
   */
  public RowGithubRepoBindingModel_ onVisibilityChanged(
      OnModelVisibilityChangedListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener) {
    onMutation();
    this.onModelVisibilityChangedListener_epoxyGeneratedModel = listener;
    return this;
  }

  public RowGithubRepoBindingModel_ model(GithubRepo model) {
    onMutation();
    this.model = model;
    return this;
  }

  public GithubRepo model() {
    return model;
  }

  public RowGithubRepoBindingModel_ isExpanded(boolean isExpanded) {
    onMutation();
    this.isExpanded = isExpanded;
    return this;
  }

  public boolean isExpanded() {
    return isExpanded;
  }

  public RowGithubRepoBindingModel_ isLastRow(boolean isLastRow) {
    onMutation();
    this.isLastRow = isLastRow;
    return this;
  }

  public boolean isLastRow() {
    return isLastRow;
  }

  public RowGithubRepoBindingModel_ avatarIndex(int avatarIndex) {
    onMutation();
    this.avatarIndex = avatarIndex;
    return this;
  }

  public int avatarIndex() {
    return avatarIndex;
  }

  /**
   * Set a click listener that will provide the parent view, model, and adapter position of the clicked view. This will clear the normal View.OnClickListener if one has been set */
  public RowGithubRepoBindingModel_ onItemClick(
      final OnModelClickListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onItemClick) {
    onMutation();
    if (onItemClick == null) {
      this.onItemClick = null;
    }
    else {
      this.onItemClick = new WrappedEpoxyModelClickListener(onItemClick);
    }
    return this;
  }

  public RowGithubRepoBindingModel_ onItemClick(View.OnClickListener onItemClick) {
    onMutation();
    this.onItemClick = onItemClick;
    return this;
  }

  public View.OnClickListener onItemClick() {
    return onItemClick;
  }

  /**
   * Set a click listener that will provide the parent view, model, and adapter position of the clicked view. This will clear the normal View.OnClickListener if one has been set */
  public RowGithubRepoBindingModel_ onDescriptionClick(
      final OnModelClickListener<RowGithubRepoBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onDescriptionClick) {
    onMutation();
    if (onDescriptionClick == null) {
      this.onDescriptionClick = null;
    }
    else {
      this.onDescriptionClick = new WrappedEpoxyModelClickListener(onDescriptionClick);
    }
    return this;
  }

  public RowGithubRepoBindingModel_ onDescriptionClick(View.OnClickListener onDescriptionClick) {
    onMutation();
    this.onDescriptionClick = onDescriptionClick;
    return this;
  }

  public View.OnClickListener onDescriptionClick() {
    return onDescriptionClick;
  }

  @Override
  public RowGithubRepoBindingModel_ id(long id) {
    super.id(id);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ id(@Nullable Number... arg0) {
    super.id(arg0);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ id(long id1, long id2) {
    super.id(id1, id2);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ id(@Nullable CharSequence arg0) {
    super.id(arg0);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ id(@Nullable CharSequence arg0,
      @Nullable CharSequence... arg1) {
    super.id(arg0, arg1);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ id(@Nullable CharSequence arg0, long arg1) {
    super.id(arg0, arg1);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ layout(@LayoutRes int arg0) {
    super.layout(arg0);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ spanSizeOverride(
      @Nullable EpoxyModel.SpanSizeOverrideCallback arg0) {
    super.spanSizeOverride(arg0);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ show() {
    super.show();
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ show(boolean show) {
    super.show(show);
    return this;
  }

  @Override
  public RowGithubRepoBindingModel_ hide() {
    super.hide();
    return this;
  }

  @Override
  @LayoutRes
  protected int getDefaultLayout() {
    return R.layout.row_github_repo;
  }

  @Override
  protected void setDataBindingVariables(ViewDataBinding binding) {
    if (!binding.setVariable(BR.model, model)) {
      throw new IllegalStateException("The attribute model was defined in your data binding model (com.airbnb.epoxy.DataBindingEpoxyModel) but a data variable of that name was not found in the layout.");
    }
    if (!binding.setVariable(BR.isExpanded, isExpanded)) {
      throw new IllegalStateException("The attribute isExpanded was defined in your data binding model (com.airbnb.epoxy.DataBindingEpoxyModel) but a data variable of that name was not found in the layout.");
    }
    if (!binding.setVariable(BR.isLastRow, isLastRow)) {
      throw new IllegalStateException("The attribute isLastRow was defined in your data binding model (com.airbnb.epoxy.DataBindingEpoxyModel) but a data variable of that name was not found in the layout.");
    }
    if (!binding.setVariable(BR.avatarIndex, avatarIndex)) {
      throw new IllegalStateException("The attribute avatarIndex was defined in your data binding model (com.airbnb.epoxy.DataBindingEpoxyModel) but a data variable of that name was not found in the layout.");
    }
    if (!binding.setVariable(BR.onItemClick, onItemClick)) {
      throw new IllegalStateException("The attribute onItemClick was defined in your data binding model (com.airbnb.epoxy.DataBindingEpoxyModel) but a data variable of that name was not found in the layout.");
    }
    if (!binding.setVariable(BR.onDescriptionClick, onDescriptionClick)) {
      throw new IllegalStateException("The attribute onDescriptionClick was defined in your data binding model (com.airbnb.epoxy.DataBindingEpoxyModel) but a data variable of that name was not found in the layout.");
    }
  }

  @Override
  protected void setDataBindingVariables(ViewDataBinding binding, EpoxyModel previousModel) {
    if (!(previousModel instanceof RowGithubRepoBindingModel_)) {
      setDataBindingVariables(binding);
      return;
    }
    RowGithubRepoBindingModel_ that = (RowGithubRepoBindingModel_) previousModel;
    if ((model != null ? !model.equals(that.model) : that.model != null)) {
      binding.setVariable(BR.model, model);
    }
    if ((isExpanded != that.isExpanded)) {
      binding.setVariable(BR.isExpanded, isExpanded);
    }
    if ((isLastRow != that.isLastRow)) {
      binding.setVariable(BR.isLastRow, isLastRow);
    }
    if ((avatarIndex != that.avatarIndex)) {
      binding.setVariable(BR.avatarIndex, avatarIndex);
    }
    if (((onItemClick == null) != (that.onItemClick == null))) {
      binding.setVariable(BR.onItemClick, onItemClick);
    }
    if (((onDescriptionClick == null) != (that.onDescriptionClick == null))) {
      binding.setVariable(BR.onDescriptionClick, onDescriptionClick);
    }
  }

  @Override
  public RowGithubRepoBindingModel_ reset() {
    onModelBoundListener_epoxyGeneratedModel = null;
    onModelUnboundListener_epoxyGeneratedModel = null;
    onModelVisibilityStateChangedListener_epoxyGeneratedModel = null;
    onModelVisibilityChangedListener_epoxyGeneratedModel = null;
    this.model = null;
    this.isExpanded = false;
    this.isLastRow = false;
    this.avatarIndex = 0;
    this.onItemClick = null;
    this.onDescriptionClick = null;
    super.reset();
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof RowGithubRepoBindingModel_)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    RowGithubRepoBindingModel_ that = (RowGithubRepoBindingModel_) o;
    if (((onModelBoundListener_epoxyGeneratedModel == null) != (that.onModelBoundListener_epoxyGeneratedModel == null))) {
      return false;
    }
    if (((onModelUnboundListener_epoxyGeneratedModel == null) != (that.onModelUnboundListener_epoxyGeneratedModel == null))) {
      return false;
    }
    if (((onModelVisibilityStateChangedListener_epoxyGeneratedModel == null) != (that.onModelVisibilityStateChangedListener_epoxyGeneratedModel == null))) {
      return false;
    }
    if (((onModelVisibilityChangedListener_epoxyGeneratedModel == null) != (that.onModelVisibilityChangedListener_epoxyGeneratedModel == null))) {
      return false;
    }
    if ((model != null ? !model.equals(that.model) : that.model != null)) {
      return false;
    }
    if ((isExpanded != that.isExpanded)) {
      return false;
    }
    if ((isLastRow != that.isLastRow)) {
      return false;
    }
    if ((avatarIndex != that.avatarIndex)) {
      return false;
    }
    if (((onItemClick == null) != (that.onItemClick == null))) {
      return false;
    }
    if (((onDescriptionClick == null) != (that.onDescriptionClick == null))) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (onModelBoundListener_epoxyGeneratedModel != null ? 1 : 0);
    result = 31 * result + (onModelUnboundListener_epoxyGeneratedModel != null ? 1 : 0);
    result = 31 * result + (onModelVisibilityStateChangedListener_epoxyGeneratedModel != null ? 1 : 0);
    result = 31 * result + (onModelVisibilityChangedListener_epoxyGeneratedModel != null ? 1 : 0);
    result = 31 * result + (model != null ? model.hashCode() : 0);
    result = 31 * result + (isExpanded ? 1 : 0);
    result = 31 * result + (isLastRow ? 1 : 0);
    result = 31 * result + avatarIndex;
    result = 31 * result + (onItemClick != null ? 1 : 0);
    result = 31 * result + (onDescriptionClick != null ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "RowGithubRepoBindingModel_{" +
        "model=" + model +
        ", isExpanded=" + isExpanded +
        ", isLastRow=" + isLastRow +
        ", avatarIndex=" + avatarIndex +
        ", onItemClick=" + onItemClick +
        ", onDescriptionClick=" + onDescriptionClick +
        "}" + super.toString();
  }
}
