package com.gojek.assignment.github.trending;

import android.view.View;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.OnModelBoundListener;
import com.airbnb.epoxy.OnModelClickListener;
import com.airbnb.epoxy.OnModelUnboundListener;
import com.airbnb.epoxy.OnModelVisibilityChangedListener;
import com.airbnb.epoxy.OnModelVisibilityStateChangedListener;
import java.lang.CharSequence;
import java.lang.Number;
import java.lang.String;

public interface RowMainErrorBindingModelBuilder {
  RowMainErrorBindingModelBuilder onBind(
      OnModelBoundListener<RowMainErrorBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowMainErrorBindingModelBuilder onUnbind(
      OnModelUnboundListener<RowMainErrorBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowMainErrorBindingModelBuilder onVisibilityStateChanged(
      OnModelVisibilityStateChangedListener<RowMainErrorBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowMainErrorBindingModelBuilder onVisibilityChanged(
      OnModelVisibilityChangedListener<RowMainErrorBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowMainErrorBindingModelBuilder error(String error);

  RowMainErrorBindingModelBuilder onRetryClick(
      final OnModelClickListener<RowMainErrorBindingModel_, DataBindingEpoxyModel.DataBindingHolder> onRetryClick);

  RowMainErrorBindingModelBuilder onRetryClick(View.OnClickListener onRetryClick);

  RowMainErrorBindingModelBuilder id(long id);

  RowMainErrorBindingModelBuilder id(@Nullable Number... arg0);

  RowMainErrorBindingModelBuilder id(long id1, long id2);

  RowMainErrorBindingModelBuilder id(@Nullable CharSequence arg0);

  RowMainErrorBindingModelBuilder id(@Nullable CharSequence arg0, @Nullable CharSequence... arg1);

  RowMainErrorBindingModelBuilder id(@Nullable CharSequence arg0, long arg1);

  RowMainErrorBindingModelBuilder layout(@LayoutRes int arg0);

  RowMainErrorBindingModelBuilder spanSizeOverride(
      @Nullable EpoxyModel.SpanSizeOverrideCallback arg0);
}
