package com.gojek.assignment.github.trending.databinding;
import com.gojek.assignment.github.trending.R;
import com.gojek.assignment.github.trending.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class RowGithubRepoLoadingBindingImpl extends RowGithubRepoLoadingBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.skeletonView, 4);
        sViewsWithIds.put(R.id.divider, 5);
    }
    // views
    @NonNull
    private final com.facebook.shimmer.ShimmerFrameLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public RowGithubRepoLoadingBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private RowGithubRepoLoadingBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.view.View) bindings[1]
            , (android.view.View) bindings[5]
            , (android.view.View) bindings[2]
            , (android.view.View) bindings[3]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            );
        this.avatar.setTag(null);
        this.line.setTag(null);
        this.line2.setTag(null);
        this.mboundView0 = (com.facebook.shimmer.ShimmerFrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        if ((dirtyFlags & 0x1L) != 0) {
            // api target 1

            com.gojek.assignment.github.trending.core.databinding.AdaptersKt.setShapeDrawableBgColor(this.avatar, "#D3D3D3");
            com.gojek.assignment.github.trending.core.databinding.AdaptersKt.setShapeDrawableBgColor(this.line, "#D3D3D3");
            com.gojek.assignment.github.trending.core.databinding.AdaptersKt.setShapeDrawableBgColor(this.line2, "#D3D3D3");
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}