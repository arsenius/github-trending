package com.gojek.assignment.github.trending;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.OnModelBoundListener;
import com.airbnb.epoxy.OnModelUnboundListener;
import com.airbnb.epoxy.OnModelVisibilityChangedListener;
import com.airbnb.epoxy.OnModelVisibilityStateChangedListener;
import java.lang.CharSequence;
import java.lang.Number;

public interface RowGithubRepoLoadingBindingModelBuilder {
  RowGithubRepoLoadingBindingModelBuilder onBind(
      OnModelBoundListener<RowGithubRepoLoadingBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoLoadingBindingModelBuilder onUnbind(
      OnModelUnboundListener<RowGithubRepoLoadingBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoLoadingBindingModelBuilder onVisibilityStateChanged(
      OnModelVisibilityStateChangedListener<RowGithubRepoLoadingBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoLoadingBindingModelBuilder onVisibilityChanged(
      OnModelVisibilityChangedListener<RowGithubRepoLoadingBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowGithubRepoLoadingBindingModelBuilder id(long id);

  RowGithubRepoLoadingBindingModelBuilder id(@Nullable Number... arg0);

  RowGithubRepoLoadingBindingModelBuilder id(long id1, long id2);

  RowGithubRepoLoadingBindingModelBuilder id(@Nullable CharSequence arg0);

  RowGithubRepoLoadingBindingModelBuilder id(@Nullable CharSequence arg0,
      @Nullable CharSequence... arg1);

  RowGithubRepoLoadingBindingModelBuilder id(@Nullable CharSequence arg0, long arg1);

  RowGithubRepoLoadingBindingModelBuilder layout(@LayoutRes int arg0);

  RowGithubRepoLoadingBindingModelBuilder spanSizeOverride(
      @Nullable EpoxyModel.SpanSizeOverrideCallback arg0);
}
