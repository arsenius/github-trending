@file:Suppress("DEPRECATION")

package com.gojek.assignment.github.trending

import com.airbnb.epoxy.EpoxyController
import kotlin.Suppress
import kotlin.Unit

inline fun EpoxyController.rowMainError(modelInitializer: RowMainErrorBindingModelBuilder.() ->
        Unit) {
    RowMainErrorBindingModel_().apply  {
        modelInitializer()
    }
    .addTo(this)
}

inline fun EpoxyController.rowGithubRepo(modelInitializer: RowGithubRepoBindingModelBuilder.() ->
        Unit) {
    RowGithubRepoBindingModel_().apply  {
        modelInitializer()
    }
    .addTo(this)
}

inline fun EpoxyController.rowEmpty(modelInitializer: RowEmptyBindingModelBuilder.() -> Unit) {
    RowEmptyBindingModel_().apply  {
        modelInitializer()
    }
    .addTo(this)
}

inline fun EpoxyController.rowGithubRepoLoading(modelInitializer:
        RowGithubRepoLoadingBindingModelBuilder.() -> Unit) {
    RowGithubRepoLoadingBindingModel_().apply  {
        modelInitializer()
    }
    .addTo(this)
}
