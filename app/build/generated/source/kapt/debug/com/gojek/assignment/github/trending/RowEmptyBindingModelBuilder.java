package com.gojek.assignment.github.trending;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.OnModelBoundListener;
import com.airbnb.epoxy.OnModelUnboundListener;
import com.airbnb.epoxy.OnModelVisibilityChangedListener;
import com.airbnb.epoxy.OnModelVisibilityStateChangedListener;
import java.lang.CharSequence;
import java.lang.Number;

public interface RowEmptyBindingModelBuilder {
  RowEmptyBindingModelBuilder onBind(
      OnModelBoundListener<RowEmptyBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowEmptyBindingModelBuilder onUnbind(
      OnModelUnboundListener<RowEmptyBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowEmptyBindingModelBuilder onVisibilityStateChanged(
      OnModelVisibilityStateChangedListener<RowEmptyBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowEmptyBindingModelBuilder onVisibilityChanged(
      OnModelVisibilityChangedListener<RowEmptyBindingModel_, DataBindingEpoxyModel.DataBindingHolder> listener);

  RowEmptyBindingModelBuilder id(long id);

  RowEmptyBindingModelBuilder id(@Nullable Number... arg0);

  RowEmptyBindingModelBuilder id(long id1, long id2);

  RowEmptyBindingModelBuilder id(@Nullable CharSequence arg0);

  RowEmptyBindingModelBuilder id(@Nullable CharSequence arg0, @Nullable CharSequence... arg1);

  RowEmptyBindingModelBuilder id(@Nullable CharSequence arg0, long arg1);

  RowEmptyBindingModelBuilder layout(@LayoutRes int arg0);

  RowEmptyBindingModelBuilder spanSizeOverride(@Nullable EpoxyModel.SpanSizeOverrideCallback arg0);
}
