package com.gojek.assignment.github.trending.databinding;
import com.gojek.assignment.github.trending.R;
import com.gojek.assignment.github.trending.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class RowGithubRepoBindingImpl extends RowGithubRepoBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public RowGithubRepoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private RowGithubRepoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (com.facebook.drawee.view.SimpleDraweeView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (android.view.View) bindings[11]
            , (android.view.View) bindings[12]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[9]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (android.view.View) bindings[5]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            );
        this.author.setTag(null);
        this.avatar.setTag(null);
        this.description.setTag(null);
        this.divider.setTag(null);
        this.expandedDivider.setTag(null);
        this.fork.setTag(null);
        this.forks.setTag(null);
        this.githubRepoView.setTag(null);
        this.language.setTag(null);
        this.languageColor.setTag(null);
        this.name.setTag(null);
        this.star.setTag(null);
        this.stars.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((com.gojek.assignment.github.trending.data.model.GithubRepo) variable);
        }
        else if (BR.isLastRow == variableId) {
            setIsLastRow((boolean) variable);
        }
        else if (BR.onItemClick == variableId) {
            setOnItemClick((android.view.View.OnClickListener) variable);
        }
        else if (BR.isExpanded == variableId) {
            setIsExpanded((boolean) variable);
        }
        else if (BR.onDescriptionClick == variableId) {
            setOnDescriptionClick((android.view.View.OnClickListener) variable);
        }
        else if (BR.avatarIndex == variableId) {
            setAvatarIndex((int) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.gojek.assignment.github.trending.data.model.GithubRepo Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setIsLastRow(boolean IsLastRow) {
        this.mIsLastRow = IsLastRow;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.isLastRow);
        super.requestRebind();
    }
    public void setOnItemClick(@Nullable android.view.View.OnClickListener OnItemClick) {
        this.mOnItemClick = OnItemClick;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onItemClick);
        super.requestRebind();
    }
    public void setIsExpanded(boolean IsExpanded) {
        this.mIsExpanded = IsExpanded;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.isExpanded);
        super.requestRebind();
    }
    public void setOnDescriptionClick(@Nullable android.view.View.OnClickListener OnDescriptionClick) {
        this.mOnDescriptionClick = OnDescriptionClick;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.onDescriptionClick);
        super.requestRebind();
    }
    public void setAvatarIndex(int AvatarIndex) {
        this.mAvatarIndex = AvatarIndex;
        synchronized(this) {
            mDirtyFlags |= 0x20L;
        }
        notifyPropertyChanged(BR.avatarIndex);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.gojek.assignment.github.trending.data.model.GithubRepo model = mModel;
        boolean isLastRowBooleanTrueIsExpanded = false;
        int isExpandedBooleanTrueModelLanguageColorJavaLangObjectNullViewGONEViewVISIBLE = 0;
        java.lang.String modelAuthor = null;
        java.lang.String modelStars = null;
        boolean isExpanded = false;
        int isLastRowBooleanTrueIsExpandedViewINVISIBLEViewVISIBLE = 0;
        boolean modelLanguageJavaLangObjectNull = false;
        java.lang.String stringFormatJavaLangString0fDoubleValueOfModelForks = null;
        boolean isExpandedBooleanTrueModelLanguageColorJavaLangObjectNull = false;
        java.lang.String modelLanguage = null;
        int IsLastRowBooleanTrueIsExpandedViewINVISIBLEViewVISIBLE1 = 0;
        java.util.List<com.gojek.assignment.github.trending.data.model.GithubRepo.BuiltBy> modelBuiltBy = null;
        java.lang.String modelBuiltByAvatarIndexAvatarUrl = null;
        boolean avatarIndexInt0 = false;
        int isExpandedViewGONEViewVISIBLE = 0;
        java.lang.Double doubleValueOfModelForks = null;
        boolean isExpandedBooleanTrueModelLanguageJavaLangObjectNull = false;
        boolean isLastRow = mIsLastRow;
        java.lang.String stringFormatJavaLangString0fDoubleValueOfModelStars = null;
        java.lang.String modelLanguageColor = null;
        java.lang.Double doubleValueOfModelStars = null;
        int modelBuiltBySize = 0;
        java.lang.String avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalseModelBuiltByAvatarIndexAvatarUrlJavaLangString = null;
        com.gojek.assignment.github.trending.data.model.GithubRepo.BuiltBy modelBuiltByAvatarIndex = null;
        java.lang.String modelDescription = null;
        java.lang.String modelName = null;
        android.view.View.OnClickListener onItemClick = mOnItemClick;
        int isExpandedBooleanTrueModelLanguageJavaLangObjectNullViewGONEViewVISIBLE = 0;
        java.lang.String modelForks = null;
        boolean modelLanguageColorJavaLangObjectNull = false;
        boolean IsExpanded1 = mIsExpanded;
        android.view.View.OnClickListener onDescriptionClick = mOnDescriptionClick;
        int avatarIndex = mAvatarIndex;
        boolean modelBuiltBySizeAvatarIndex = false;
        boolean avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalse = false;
        boolean IsLastRowBooleanTrueIsExpanded1 = false;

        if ((dirtyFlags & 0x41L) != 0) {



                if (model != null) {
                    // read model.author
                    modelAuthor = model.getAuthor();
                    // read model.stars
                    modelStars = model.getStars();
                    // read model.language
                    modelLanguage = model.getLanguage();
                    // read model.languageColor
                    modelLanguageColor = model.getLanguageColor();
                    // read model.description
                    modelDescription = model.getDescription();
                    // read model.name
                    modelName = model.getName();
                    // read model.forks
                    modelForks = model.getForks();
                }


                // read Double.valueOf(model.stars)
                doubleValueOfModelStars = java.lang.Double.valueOf(modelStars);
                // read Double.valueOf(model.forks)
                doubleValueOfModelForks = java.lang.Double.valueOf(modelForks);


                // read String.format("%,.0f", Double.valueOf(model.stars))
                stringFormatJavaLangString0fDoubleValueOfModelStars = java.lang.String.format("%,.0f", doubleValueOfModelStars);
                // read String.format("%,.0f", Double.valueOf(model.forks))
                stringFormatJavaLangString0fDoubleValueOfModelForks = java.lang.String.format("%,.0f", doubleValueOfModelForks);
        }
        if ((dirtyFlags & 0x4aL) != 0) {

            if((dirtyFlags & 0x4aL) != 0) {
                if(isLastRow) {
                        dirtyFlags |= 0x100L;
                        dirtyFlags |= 0x10000000L;
                }
                else {
                        dirtyFlags |= 0x80L;
                        dirtyFlags |= 0x8000000L;
                }
            }
        }
        if ((dirtyFlags & 0x44L) != 0) {
        }
        if ((dirtyFlags & 0x49L) != 0) {



                // read !isExpanded
                isExpanded = !IsExpanded1;
            if((dirtyFlags & 0x49L) != 0) {
                if(isExpanded) {
                        dirtyFlags |= 0x4000L;
                        dirtyFlags |= 0x100000L;
                }
                else {
                        dirtyFlags |= 0x2000L;
                        dirtyFlags |= 0x80000L;
                }
            }
            if((dirtyFlags & 0x48L) != 0) {
                if(isExpanded) {
                        dirtyFlags |= 0x40000L;
                }
                else {
                        dirtyFlags |= 0x20000L;
                }
            }

            if ((dirtyFlags & 0x48L) != 0) {

                    // read !isExpanded ? View.GONE : View.VISIBLE
                    isExpandedViewGONEViewVISIBLE = ((isExpanded) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
        }
        if ((dirtyFlags & 0x50L) != 0) {
        }
        if ((dirtyFlags & 0x61L) != 0) {



                // read avatarIndex >= 0
                avatarIndexInt0 = (avatarIndex) >= (0);
            if((dirtyFlags & 0x61L) != 0) {
                if(avatarIndexInt0) {
                        dirtyFlags |= 0x4000000L;
                }
                else {
                        dirtyFlags |= 0x2000000L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x4000000L) != 0) {



                if (model != null) {
                    // read model.builtBy
                    modelBuiltBy = model.getBuiltBy();
                }


                if (modelBuiltBy != null) {
                    // read model.builtBy.size()
                    modelBuiltBySize = modelBuiltBy.size();
                }


                // read model.builtBy.size() > avatarIndex
                modelBuiltBySizeAvatarIndex = (modelBuiltBySize) > (avatarIndex);
        }
        if ((dirtyFlags & 0x8000080L) != 0) {


            if ((dirtyFlags & 0x80L) != 0) {

                    // read !isExpanded
                    isExpanded = !IsExpanded1;
                if((dirtyFlags & 0x49L) != 0) {
                    if(isExpanded) {
                            dirtyFlags |= 0x4000L;
                            dirtyFlags |= 0x100000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                            dirtyFlags |= 0x80000L;
                    }
                }
                if((dirtyFlags & 0x48L) != 0) {
                    if(isExpanded) {
                            dirtyFlags |= 0x40000L;
                    }
                    else {
                            dirtyFlags |= 0x20000L;
                    }
                }
            }
        }

        if ((dirtyFlags & 0x4aL) != 0) {

                // read isLastRow ? true : !isExpanded
                isLastRowBooleanTrueIsExpanded = ((isLastRow) ? (true) : (isExpanded));
                // read isLastRow ? true : isExpanded
                IsLastRowBooleanTrueIsExpanded1 = ((isLastRow) ? (true) : (IsExpanded1));
            if((dirtyFlags & 0x4aL) != 0) {
                if(isLastRowBooleanTrueIsExpanded) {
                        dirtyFlags |= 0x10000L;
                }
                else {
                        dirtyFlags |= 0x8000L;
                }
            }
            if((dirtyFlags & 0x4aL) != 0) {
                if(IsLastRowBooleanTrueIsExpanded1) {
                        dirtyFlags |= 0x1000L;
                }
                else {
                        dirtyFlags |= 0x800L;
                }
            }


                // read isLastRow ? true : !isExpanded ? View.INVISIBLE : View.VISIBLE
                IsLastRowBooleanTrueIsExpandedViewINVISIBLEViewVISIBLE1 = ((isLastRowBooleanTrueIsExpanded) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
                // read isLastRow ? true : isExpanded ? View.INVISIBLE : View.VISIBLE
                isLastRowBooleanTrueIsExpandedViewINVISIBLEViewVISIBLE = ((IsLastRowBooleanTrueIsExpanded1) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
        }
        if ((dirtyFlags & 0x61L) != 0) {

                // read avatarIndex >= 0 ? model.builtBy.size() > avatarIndex : false
                avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalse = ((avatarIndexInt0) ? (modelBuiltBySizeAvatarIndex) : (false));
            if((dirtyFlags & 0x61L) != 0) {
                if(avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalse) {
                        dirtyFlags |= 0x400000L;
                }
                else {
                        dirtyFlags |= 0x200000L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x482000L) != 0) {


            if ((dirtyFlags & 0x80000L) != 0) {

                    if (model != null) {
                        // read model.language
                        modelLanguage = model.getLanguage();
                    }


                    // read model.language == null
                    modelLanguageJavaLangObjectNull = (modelLanguage) == (null);
            }
            if ((dirtyFlags & 0x400000L) != 0) {

                    if (model != null) {
                        // read model.builtBy
                        modelBuiltBy = model.getBuiltBy();
                    }


                    if (modelBuiltBy != null) {
                        // read model.builtBy[avatarIndex]
                        modelBuiltByAvatarIndex = getFromList(modelBuiltBy, avatarIndex);
                    }


                    if (modelBuiltByAvatarIndex != null) {
                        // read model.builtBy[avatarIndex].avatarUrl
                        modelBuiltByAvatarIndexAvatarUrl = modelBuiltByAvatarIndex.getAvatarUrl();
                    }
            }
            if ((dirtyFlags & 0x2000L) != 0) {

                    if (model != null) {
                        // read model.languageColor
                        modelLanguageColor = model.getLanguageColor();
                    }


                    // read model.languageColor == null
                    modelLanguageColorJavaLangObjectNull = (modelLanguageColor) == (null);
            }
        }

        if ((dirtyFlags & 0x49L) != 0) {

                // read !isExpanded ? true : model.languageColor == null
                isExpandedBooleanTrueModelLanguageColorJavaLangObjectNull = ((isExpanded) ? (true) : (modelLanguageColorJavaLangObjectNull));
                // read !isExpanded ? true : model.language == null
                isExpandedBooleanTrueModelLanguageJavaLangObjectNull = ((isExpanded) ? (true) : (modelLanguageJavaLangObjectNull));
            if((dirtyFlags & 0x49L) != 0) {
                if(isExpandedBooleanTrueModelLanguageColorJavaLangObjectNull) {
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x200L;
                }
            }
            if((dirtyFlags & 0x49L) != 0) {
                if(isExpandedBooleanTrueModelLanguageJavaLangObjectNull) {
                        dirtyFlags |= 0x1000000L;
                }
                else {
                        dirtyFlags |= 0x800000L;
                }
            }


                // read !isExpanded ? true : model.languageColor == null ? View.GONE : View.VISIBLE
                isExpandedBooleanTrueModelLanguageColorJavaLangObjectNullViewGONEViewVISIBLE = ((isExpandedBooleanTrueModelLanguageColorJavaLangObjectNull) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read !isExpanded ? true : model.language == null ? View.GONE : View.VISIBLE
                isExpandedBooleanTrueModelLanguageJavaLangObjectNullViewGONEViewVISIBLE = ((isExpandedBooleanTrueModelLanguageJavaLangObjectNull) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        if ((dirtyFlags & 0x61L) != 0) {

                // read avatarIndex >= 0 ? model.builtBy.size() > avatarIndex : false ? model.builtBy[avatarIndex].avatarUrl : ""
                avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalseModelBuiltByAvatarIndexAvatarUrlJavaLangString = ((avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalse) ? (modelBuiltByAvatarIndexAvatarUrl) : (""));
        }
        // batch finished
        if ((dirtyFlags & 0x41L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.author, modelAuthor);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.description, modelDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.forks, stringFormatJavaLangString0fDoubleValueOfModelForks);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.language, modelLanguage);
            com.gojek.assignment.github.trending.core.databinding.AdaptersKt.setShapeDrawableBgColor(this.languageColor, modelLanguageColor);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.name, modelName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.stars, stringFormatJavaLangString0fDoubleValueOfModelStars);
        }
        if ((dirtyFlags & 0x44L) != 0) {
            // api target 1

            this.author.setOnClickListener(onItemClick);
            this.avatar.setOnClickListener(onItemClick);
            this.githubRepoView.setOnClickListener(onItemClick);
            this.name.setOnClickListener(onItemClick);
        }
        if ((dirtyFlags & 0x61L) != 0) {
            // api target 1

            com.gojek.assignment.github.trending.core.databinding.AdaptersKt.setImageUrl(this.avatar, avatarIndexInt0ModelBuiltBySizeAvatarIndexBooleanFalseModelBuiltByAvatarIndexAvatarUrlJavaLangString);
        }
        if ((dirtyFlags & 0x48L) != 0) {
            // api target 1

            this.description.setVisibility(isExpandedViewGONEViewVISIBLE);
            this.fork.setVisibility(isExpandedViewGONEViewVISIBLE);
            this.forks.setVisibility(isExpandedViewGONEViewVISIBLE);
            this.star.setVisibility(isExpandedViewGONEViewVISIBLE);
            this.stars.setVisibility(isExpandedViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x50L) != 0) {
            // api target 1

            this.description.setOnClickListener(onDescriptionClick);
        }
        if ((dirtyFlags & 0x4aL) != 0) {
            // api target 1

            this.divider.setVisibility(isLastRowBooleanTrueIsExpandedViewINVISIBLEViewVISIBLE);
            this.expandedDivider.setVisibility(IsLastRowBooleanTrueIsExpandedViewINVISIBLEViewVISIBLE1);
        }
        if ((dirtyFlags & 0x49L) != 0) {
            // api target 1

            this.language.setVisibility(isExpandedBooleanTrueModelLanguageJavaLangObjectNullViewGONEViewVISIBLE);
            this.languageColor.setVisibility(isExpandedBooleanTrueModelLanguageColorJavaLangObjectNullViewGONEViewVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): isLastRow
        flag 2 (0x3L): onItemClick
        flag 3 (0x4L): isExpanded
        flag 4 (0x5L): onDescriptionClick
        flag 5 (0x6L): avatarIndex
        flag 6 (0x7L): null
        flag 7 (0x8L): isLastRow ? true : !isExpanded
        flag 8 (0x9L): isLastRow ? true : !isExpanded
        flag 9 (0xaL): !isExpanded ? true : model.languageColor == null ? View.GONE : View.VISIBLE
        flag 10 (0xbL): !isExpanded ? true : model.languageColor == null ? View.GONE : View.VISIBLE
        flag 11 (0xcL): isLastRow ? true : isExpanded ? View.INVISIBLE : View.VISIBLE
        flag 12 (0xdL): isLastRow ? true : isExpanded ? View.INVISIBLE : View.VISIBLE
        flag 13 (0xeL): !isExpanded ? true : model.languageColor == null
        flag 14 (0xfL): !isExpanded ? true : model.languageColor == null
        flag 15 (0x10L): isLastRow ? true : !isExpanded ? View.INVISIBLE : View.VISIBLE
        flag 16 (0x11L): isLastRow ? true : !isExpanded ? View.INVISIBLE : View.VISIBLE
        flag 17 (0x12L): !isExpanded ? View.GONE : View.VISIBLE
        flag 18 (0x13L): !isExpanded ? View.GONE : View.VISIBLE
        flag 19 (0x14L): !isExpanded ? true : model.language == null
        flag 20 (0x15L): !isExpanded ? true : model.language == null
        flag 21 (0x16L): avatarIndex >= 0 ? model.builtBy.size() > avatarIndex : false ? model.builtBy[avatarIndex].avatarUrl : ""
        flag 22 (0x17L): avatarIndex >= 0 ? model.builtBy.size() > avatarIndex : false ? model.builtBy[avatarIndex].avatarUrl : ""
        flag 23 (0x18L): !isExpanded ? true : model.language == null ? View.GONE : View.VISIBLE
        flag 24 (0x19L): !isExpanded ? true : model.language == null ? View.GONE : View.VISIBLE
        flag 25 (0x1aL): avatarIndex >= 0 ? model.builtBy.size() > avatarIndex : false
        flag 26 (0x1bL): avatarIndex >= 0 ? model.builtBy.size() > avatarIndex : false
        flag 27 (0x1cL): isLastRow ? true : isExpanded
        flag 28 (0x1dL): isLastRow ? true : isExpanded
    flag mapping end*/
    //end
}