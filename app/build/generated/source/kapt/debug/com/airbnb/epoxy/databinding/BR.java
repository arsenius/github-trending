package com.airbnb.epoxy.databinding;

public class BR {
  public static final int _all = 0;

  public static final int isExpanded = 1;

  public static final int onRetryClick = 2;

  public static final int onDescriptionClick = 3;

  public static final int isLastRow = 4;

  public static final int model = 5;

  public static final int onItemClick = 6;

  public static final int avatarIndex = 7;

  public static final int error = 8;
}
